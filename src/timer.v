`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
//
// Create Date:		15:10:33 01/30/2014
// Design Name:		MCU
// Module Name:		timer
// Project Name:	dsl_group_d
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		The timer for the MCU.
//
// Dependencies:	none
//
// Revision:
// v0.02 - 20140131xxxxZ - Implementation complete
// v0.01 - 201401312033Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////
`include "globals.v"

module timer
(
	input			CLK,
	input			RESET,
	/* bus signals */
	inout	[7:0]	BUS_DATA,
	input	[7:0]	BUS_ADDR,
	input			BUS_DATA_OE,
	output			BUS_IRQ,
	input			BUS_ACK
);

	parameter	[7:0]	timer_base_addr			= 8'hF0; /* base address */
	parameter	[7:0]	timer_irq_rate_default	= 100; /* irq rate @100ms */
	parameter			timer_irq_en_default	= `TRUE; /* default: IRQ on */
	wire	[7:0]	BUS_DATA_IN;
	reg		[7:0]	timer_irq_rate;
	reg				timer_irq_en;
	reg		[31:0]	downscaler;
	reg		[31:0]	timer;
	reg				timer_target_irq;
	reg		[31:0]	timer_prev_irq;
	reg				timer_irq;
	reg				timer_oe;
	
	assign BUS_DATA_IN = BUS_DATA;
	assign BUS_DATA = timer_oe ? timer[7:0] : 8'hZZ;
	assign BUS_IRQ = timer_irq;
	
	/*
	 * timer_base_addr + 0 ==> current timer value
	 * timer_base_addr + 1 ==> IRQ rate register (initially set to 100ms)
	 * timer_base_addr + 2 ==> reset timer
	 * timer_base_addr + 3 ==> IRQ enable register
	 */

	/* the IRQ is raised when the time slice is over. The next IRQ will take
	 * place after (by default) 100ms, reconfigurable using IRQ rate register
	 * defined below
	 */
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			timer_irq_rate		<= timer_irq_rate_default;
		end
		else if( ( BUS_ADDR == timer_base_addr + 8'h01 ) & BUS_DATA_OE ) begin
			timer_irq_rate		<= BUS_DATA_IN;
		end
	end

	/* IRQ enable register. If low, no IRQs will be raised */
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			timer_irq_en		<= timer_irq_en_default;
		end
		else if( ( BUS_ADDR == timer_base_addr + 8'h02 ) & BUS_DATA_OE ) begin
			timer_irq_en		<= BUS_DATA_IN[0];
		end
	end

	/* first create a 1 kHz waveform (1ms tick rate) */
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			downscaler			<= 32'h0000_0000;
		end
		else begin
			if( downscaler == 32'd49_999 ) begin
				downscaler		<= 32'h0000_0000;
			end
			else begin
				downscaler		<= downscaler + 32'h0000_0001; /* add 1 */
			end
		end
	end

	/* and generate a timer driven from the 1 kHz clock */
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			timer				<= 32'h0000_0000;
		end
		else begin
			if ( BUS_ADDR == timer_base_addr + 8'h02 ) begin
				timer			<= 32'h0000_0000;
			end
			else if( downscaler == 32'h0000_0000 ) begin
				timer			<= timer + 32'h0000_0001;
			end
			else begin
				timer			<= timer;
			end
		end
	end

	/* IRQ generation + last IRQ time storage */
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			timer_target_irq	<= `FALSE;
			timer_prev_irq		<= 32'h0000_0000;
		end
		else begin
		// TODO: this will potentially cause problems on enabling IRQs
		// TODO: always reset the previous interrupt time even if it was not
		// TODO: raised. +, not all paths lead to lowering the IRQ signal
			if( ( timer_prev_irq + timer_irq_rate ) == timer ) begin
				if( timer_irq_en ) begin
					timer_target_irq	<= `TRUE;
					timer_prev_irq		<= timer;
				end
			end
			else begin
				timer_target_irq		<= `FALSE;
			end
		end
	end

	/* IRQ broadcast: IRQ will not be rescinded until ACKd */
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			timer_irq			<= `FALSE;
		end
		else if( timer_target_irq ) begin
			timer_irq			<= `TRUE;
		end
		else if( BUS_ACK ) begin
			timer_irq			<= `FALSE;
		end
	end

	/* tri-state line for timer value */
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			timer_oe				<= `FALSE;
		end
		else begin
			if( ( BUS_ADDR == timer_base_addr ) & ~BUS_DATA_OE ) begin
				timer_oe			<= `TRUE;
			end
			else begin
				timer_oe			<= `FALSE;
			end
		end
	end

	

endmodule