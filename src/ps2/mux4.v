`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 		UoE
// Engineer:		Mihail Atanassov
// 
// Create Date:		10:18:11 10/17/2012
// Design Name:		PS/2 Demo
// Module Name:		Mux4x5
// Project Name:	PS/2 Demo
// Target Devices:	any
// Tool versions:	Xilinx ISE WebPack 14.2
// Description:		A 5-bit 4-input multiplexor with 2-bit select line
//
// Dependencies: 
//
// Revision: 
//			Revision 1	-	Implementation complete
//			Revision 0.01	-	File Created
// Additional Comments: 
//	Interface:
//		A,B,C,D:	Input buses.
//		S:		Select line.
//		OUT:		Output bus.
//////////////////////////////////////////////////////////////////////////////////
module mux4x5(
		input		[4:0]	A,
		input		[4:0]	B,
		input		[4:0]	C,
		input		[4:0]	D,
		input		[1:0]	S,
		output reg	[4:0]	OUT
	);

	always @( * ) begin
		case(S)
			
			2'b00:		OUT <= A;
			2'b01:		OUT <= B;
			2'b10:		OUT <= C;
			2'b11:		OUT <= D;
			default:	OUT <= 5'd0;
			
		endcase	
	end

endmodule
