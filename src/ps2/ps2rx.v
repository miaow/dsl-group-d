`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
//
// Create Date:		09:18:33 01/23/2014
// Design Name:		PS/2 Demo
// Module Name:		ps2rx
// Project Name:	PS/2 Demo
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		The PS/2 receiver module.
//
// Dependencies:	globals.v
//
// Revision:
// v1.00 - 201402081904Z - Verified
// v0.02 - 201401251920Z - Implementation done
// v0.01 - 201401230918Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////
`include "../globals.v"
`define RX__STATE_START			3'b000
`define RX__STATE_READ			3'b001
`define RX__STATE_PARITY		3'b010
`define RX__STATE_STOP			3'b011
`define RX__STATE_FRAME_RECV	3'b100
`define RX__STATE_DEFAULT		default

module ps2rx
(
    input				RESET,
    input				CLK,
    input				IO_CLK_IN,
    input				IO_DAT_IN,
    input				RECV_EN,
    output	[7:0]		BYTE,
    output	[1:0]		BYTE_ERR,
    output				RECV_OK
);

	/* buffer the i/o clock in order to detect clock edges */
	reg buf__IO_CLK_IN;
	always @(posedge CLK)
		buf__IO_CLK_IN 						<= IO_CLK_IN;


	/* state machine handling the 11-bit words incoming */
	reg		[2:0]		curr__state,			next__state;
	reg		[7:0]		curr__DAT_SHIFTREG,		next__DAT_SHIFTREG;
	reg		[3:0]		curr__bit_in,			next__bit_in;
	reg					curr__RECV_OK,			next__RECV_OK;
	reg		[1:0]		curr__BYTE_ERR,			next__BYTE_ERR;
	reg		[15:0]		curr__timeout_count,	next__timeout_count;
	reg		[7:0]		buf__BYTE; /* output buffer */
	reg		[1:0]		buf__BYTE_ERR; /* output buffer */

	/* update registers */
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			curr__state						<= 3'b000;
			curr__DAT_SHIFTREG				<= 8'h00;
			curr__bit_in					<= 4'h0;
			curr__RECV_OK					<= 1'b0;
			curr__BYTE_ERR					<= 2'b00;
			curr__timeout_count				<= 16'h0000;
			buf__BYTE						<= 8'h00;
			buf__BYTE_ERR					<= 2'b00;
		end
		else begin
			curr__state						<= next__state;
			curr__DAT_SHIFTREG				<= next__DAT_SHIFTREG;
			curr__bit_in					<= next__bit_in;
			curr__RECV_OK					<= next__RECV_OK;
			curr__BYTE_ERR					<= next__BYTE_ERR;
			curr__timeout_count				<= next__timeout_count;
			if(next__RECV_OK) begin
				/* only change output when whole byte received */
				buf__BYTE					<= curr__DAT_SHIFTREG;
				buf__BYTE_ERR				<= curr__BYTE_ERR;
			end
		end
	end

	/* state machine logic */
	always @( * ) begin
		/* default values */
		next__state							= curr__state;
		next__DAT_SHIFTREG					= curr__DAT_SHIFTREG;
		next__bit_in						= curr__bit_in;
		next__RECV_OK						= `FALSE;
		next__BYTE_ERR						= curr__BYTE_ERR;
		next__timeout_count					= curr__timeout_count + 1;

		case( curr__state )

			`RX__STATE_START: begin
				/* falling edge of CLK and IO_DAT_IN is low -> start bit */
				if( RECV_EN & buf__IO_CLK_IN & ~IO_CLK_IN & ~IO_DAT_IN ) begin
					next__state				= `RX__STATE_READ;
				end
				next__bit_in				= 4'h0;
				next__BYTE_ERR				= 2'b00;
				next__timeout_count			= 16'h0000;
			end

			`RX__STATE_READ: begin
				/* read 8 successive bits */
				if( curr__timeout_count == 60_000) /* 1.2ms timeout */
					next__state				= `RX__STATE_START;
				else if( curr__bit_in == 4'h8 ) begin
					/* last bit read, go to parity check */
					next__state				= `RX__STATE_PARITY;
					next__bit_in			= 4'h0;
				end
				else if( buf__IO_CLK_IN & ~IO_CLK_IN ) begin
					/* fill up the shift register */
					next__DAT_SHIFTREG[6:0]	= curr__DAT_SHIFTREG[7:1];
					next__DAT_SHIFTREG[7]	= IO_DAT_IN;
					next__bit_in			= curr__bit_in + 1;
				end
			end

			`RX__STATE_PARITY: begin
				/* PS/2 standard specifies odd parity */
				if( curr__timeout_count == 60_000 ) /* 1.2ms */
					next__state				= `RX__STATE_START;
				else if( buf__IO_CLK_IN & ~IO_CLK_IN ) begin
					if( IO_DAT_IN != ~^curr__DAT_SHIFTREG[7:0] )
						/* parity mismatch */
						next__BYTE_ERR[0] = 1'b1;
					next__state 			= `RX__STATE_STOP;
				end
			end

			`RX__STATE_STOP: begin
				if( curr__timeout_count == 60_000 ) /* 1.2ms */
					next__state				= `RX__STATE_START;
				else if( buf__IO_CLK_IN & ~IO_CLK_IN ) begin
					if( ~IO_DAT_IN )
						/* stop bit was not high -> frame error */
						next__BYTE_ERR[1] = 1'b1;
					next__state				= `RX__STATE_FRAME_RECV;
				end
			end

			`RX__STATE_FRAME_RECV: begin
				/* the data is ready to be sent to the controller */
				next__RECV_OK				= `TRUE;
				next__state					= `RX__STATE_START;
			end

			`RX__STATE_DEFAULT: begin
				next__state					= `RX__STATE_START;
				next__DAT_SHIFTREG			= 8'h00;
				next__bit_in				= 4'h0;
				next__RECV_OK				= `FALSE;
				next__BYTE_ERR				= 2'b00;
				next__timeout_count			= 16'h0000;
			end

		endcase
	end

	assign RECV_OK							= curr__RECV_OK;
	assign BYTE								= buf__BYTE;
	assign BYTE_ERR							= buf__BYTE_ERR;

endmodule
