`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
//
// Create Date:		17:53:58 01/25/2014
// Design Name:		PS/2 Demo
// Module Name:		ps2ctrl
// Project Name:	PS/2 Demo
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		The PS/2 controller module.
//
// Dependencies:	ps2tx.v,
//					ps2rx.v,
//					globals.v
//
// Revision:
// v2.00 - 201402211500Z - Scrollwheel support added
// v1.00 - 201402081857Z - Verified
// v0.02 - 201401301842Z - Implementation done
// v0.01 - 201401251753Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////

`include "../globals.v"

/*------------------------------------------------------------------------------
 * State table for the controller
 *----------------------------------------------------------------------------*/
`define STATE__WAIT_RESET				6'h00
`define STATE__RESET					6'h01
`define STATE__WAIT_RESET_XMIT			6'h02
`define STATE__WAIT_RESET_ACK			6'h03
`define STATE__WAIT_BAT_ACK				6'h04
`define STATE__WAIT_MOUSE_ID			6'h05
/* Basic mode - no scrollwheel */
`define STATE__EN_BASIC					6'h06
`define STATE__EN_BASIC_XMIT			6'h07
`define STATE__EN_BASIC_ACK				6'h08
`define STATE__BASIC_0					6'h09
`define STATE__BASIC_1					6'h0A
`define STATE__BASIC_2					6'h0B
`define STATE__BASIC_IRQ				6'h0C
/* Scroll mode - scrollwheel enabled */
`define STATE__SCROLL_SSR_0				6'h0D
`define STATE__SCROLL_SSR_0_XMIT		6'h0E
`define STATE__SCROLL_SSR_0_ACK			6'h0F
`define STATE__SCROLL_SSR_0_VAL			6'h10
`define STATE__SCROLL_SSR_0_VAL_XMIT	6'h11
`define STATE__SCROLL_SSR_0_VAL_ACK		6'h12
`define STATE__SCROLL_SSR_1				6'h13
`define STATE__SCROLL_SSR_1_XMIT		6'h14
`define STATE__SCROLL_SSR_1_ACK			6'h15
`define STATE__SCROLL_SSR_1_VAL			6'h16
`define STATE__SCROLL_SSR_1_VAL_XMIT	6'h17
`define STATE__SCROLL_SSR_1_VAL_ACK		6'h18
`define STATE__SCROLL_SSR_2				6'h19
`define STATE__SCROLL_SSR_2_XMIT		6'h1A
`define STATE__SCROLL_SSR_2_ACK			6'h1B
`define STATE__SCROLL_SSR_2_VAL			6'h1C
`define STATE__SCROLL_SSR_2_VAL_XMIT	6'h1D
`define STATE__SCROLL_SSR_2_VAL_ACK		6'h1E
`define STATE__SCROLL_RDT				6'h1F
`define STATE__SCROLL_RDT_XMIT			6'h20
`define STATE__SCROLL_RDT_ACK			6'h21
`define STATE__SCROLL_RDT_VAL			6'h22
`define STATE__EN_SCROLL				6'h23
`define STATE__EN_SCROLL_XMIT			6'h24
`define STATE__EN_SCROLL_ACK			6'h25
`define STATE__SCROLL_0					6'h26
`define STATE__SCROLL_1					6'h27
`define STATE__SCROLL_2					6'h28
`define STATE__SCROLL_3					6'h29
`define STATE__SCROLL_IRQ				6'h2A

/* Commands and responses */
`define CMD__RESET						8'hFF
`define CMD__ENABLE						8'hF4
`define CMD__SET_SAMPLE_RATE			8'hF3
`define CMD__READ_DEVICE_TYPE			8'hF2
`define ANS__ACK						8'hFA
`define ANS__MOUSE_ID_BASIC				8'h00
`define ANS__MOUSE_ID_SCROLL			8'h03
`define ANS__BAT_PASS					8'hAA


/* TEXT SOURCE: http://www.computer-engineering.org/ps2mouse/
 * Author: Adam Chapweske
 * Last Updated: 04/01/03
 * Legal Information
 * -----------------
 * All information within this article is provided "as is" and without any
 * express or implied warranties, including, without limitation, the implied
 * warranties of merchantibility and fitness for a particular purpose.
 * This article is protected under copyright law. This document may be copied
 * only if the source, author, date, and legal information is included.
 *
 * Following is the set of commands accepted by the standard PS/2 mouse. If the
 * mouse is in stream mode, the host should disable data reporting
 * (command 0xF5) before sending any other commands.
 *
 * 0xFF (Reset) - The mouse responds to this command with "acknowledge" (0xFA)
 * 		then enters reset mode.
 * 0xFE (Resend) - The host sends this command whenever it receives invalid data
 * 		from the mouse. The mouse responds by resending the last packet it sent
 * 		to the host. If the mouse responds to the "Resend" command with another
 * 		invalid packet, the host may either issue another "Resend" command,
 * 		issue an "Error" (0xFC) command, cycle the mouse's power supply to reset
 * 		the mouse, or it may inhibit communication (by bringing the clock line
 * 		low). This command is not buffered, which means "Resend" will never be
 * 		sent in response to the "Resend" command.
 * 0xF6 (Set Defaults) - The mouse responds with "acknowledge" (0xFA) then loads
 * 		the following values: Sampling rate = 100, resolution = 4 counts/mm,
 * 		Scaling = 1:1, data reporting = disabled. The mouse then resets its
 * 		movement counters and enters stream mode.
 * 0xF5 (Disable Data Reporting) - The mouse responds with "acknowledge" (0xFA)
 * 		then disables data reporting and resets its movement counters. This only
 * 		affects data reporting in stream mode and does not disable sampling.
 * 		Disabled stream mode functions the same as remote mode.
 * 0xF4 (Enable Data Reporting) - The mouse responds with "acknowledge" (0xFA)
 * 		then enables data reporting and resets its movement counters. This
 * 		command may be issued while the mouse is in remote mode, but it will
 * 		only affect data reporting in stream mode.
 * 0xF3 (Set Sample Rate) - The mouse responds with "acknowledge" (0xFA) then
 * 		reads one more byte from the host. The mouse saves this byte as the new
 * 		sample rate. After receiving the sample rate, the mouse again responds
 * 		with "acknowledge" (0xFA) and resets its movement counters. Valid sample
 * 		rates are 10, 20, 40, 60, 80, 100, and 200 samples/sec.
 * 0xF2 (Get Device ID) - The mouse responds with "acknowledge" (0xFA) followed
 * 		by its device ID (0x00 for the standard PS/2 mouse). The mouse should
 * 		also reset its movement counters.
 * 0xF0 (Set Remote Mode) - The mouse responds with "acknowledge" (0xFA) then
 * 		resets its movement counters and enters remote mode.
 * 0xEE (Set Wrap Mode) - The mouse responds with "acknowledge" (0xFA) then
 * 		resets its movement counters and enters wrap mode.
 * 0xEC (Reset Wrap Mode) - The mouse responds with "acknowledge" (0xFA) then
 * 		resets its movement counters and enters the mode it was in prior to wrap
 * 		mode (stream mode or remote mode).
 * 0xEB (Read Data) - The mouse responds with "acknowledge" (0xFA) then sends a
 * 		movement data packet. This is the only way to read data in remote mode.
 * 		After the data packet has successfully been sent, the mouse resets its
 * 		movement counters.
 * 0xEA (Set Stream Mode) - The mouse responds with "acknowledge" (0xFA) then
 * 		resets its movement counters and enters stream mode.
 * 0xE9 (Status Request) - The mouse responds with "acknowledge" (0xFA) then
 * 		sends this 3-byte status packet and resets its movement counters:
 *
 *         Bit 7   Bit 6 Bit 5   Bit 4   Bit 3    Bit 2     Bit 1      Bit 0
 * Byte 1 Always 0 Mode  Enable Scaling Always 0 Left Btn Middle Btn Right Btn
 * Byte 2                             Resolution
 * Byte 3                             Sample Rate
 */

module ps2ctrl
(
`ifdef PS2_DEBUG
	/* debug o/p */
	output	[5:0]	DBG_STATE,
`endif
    input			CLK,
    input			RESET,
    input			EN_SCROLL,
    output			TX_XMIT,
    output	[7:0]	TX_BYTE,
    input			TX_XMIT_OK,
    output			RX_RECV_EN,
    input	[7:0]	RX_BYTE,
    input	[1:0]	RX_BYTE_ERR,
    input			RX_RECV_OK,
    output	[7:0]	DAT_STATUS,
    output	[7:0]	DAT_DX,
    output	[7:0]	DAT_DY,
    output	[3:0]	DAT_DZ,
    output			PKT_OK,
    output			IRQ,
    input			ACK
);

	/* Setup sequence:
	 * Host FF (reset)
	 * Dev  FA (ACK)
	 * Dev  AA (self-test pass)
	 * Dev  00 (Mouse ID)
	 * Basic mode:
	 * Host F4 (enable reporting)
	 * Dev  FA (ACK)
	 * If errors present, start over. Once setup, raise read enable flag.
	 *
	 * Scroll mode: after Mouse ID:
	 * Host F3 (Set Sample Rate)
	 * Dev  FA
	 * Host C8 (decimal 200)
	 * Dev  FA
	 * Host F3
	 * Dev  FA
	 * Host 64 (decimal 100)
	 * Dev  FA
	 * Host F3
	 * Dev  FA
	 * Host 50 (decimal 80)
	 * Dev  FA
	 * Host F2 (Read Device Type)
	 * Dev  FA
	 * Dev  03 (03 only if scrollwheel present, 00 otherwise)
	 * Host F4 (enable reporting)
	 * Dev  FA (ACK)
	 */

	/* State control */
	reg	[5:0]		curr__state, next__state;
	reg	[23:0]		curr__count, next__count;
	/* Tx control */
	reg				curr__TX_XMIT, next__TX_XMIT;
	reg	[7:0]		curr__TX_BYTE, next__TX_BYTE;
	/* Rx control */
	reg				curr__RX_RECV_EN, next__RX_RECV_EN;
	/* Rx data reg's */
	reg	[7:0]		curr__DAT_STATUS, next__DAT_STATUS;
	reg	[7:0]		curr__DAT_DX, next__DAT_DX;
	reg	[7:0]		curr__DAT_DY, next__DAT_DY;
	reg	[3:0]		curr__DAT_DZ, next__DAT_DZ;
	reg				curr__IRQ, next__IRQ;
	reg				curr__PKT_OK, next__PKT_OK;

	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			curr__state			<= 6'h0;
			curr__count			<= 24'h000000;
			curr__TX_XMIT		<= `FALSE;
			curr__TX_BYTE		<= 8'h00;
			curr__RX_RECV_EN	<= `FALSE;
			curr__DAT_STATUS	<= 8'h00;
			curr__DAT_DX		<= 8'h00;
			curr__DAT_DY		<= 8'h00;
			curr__DAT_DZ		<= 4'h0;
			curr__IRQ			<= `FALSE;
			curr__PKT_OK		<= `FALSE;
		end
		else begin
			curr__state			<= next__state;
			curr__count			<= next__count;
			curr__TX_XMIT		<= next__TX_XMIT;
			curr__TX_BYTE		<= next__TX_BYTE;
			curr__RX_RECV_EN	<= next__RX_RECV_EN;
			curr__DAT_STATUS	<= next__DAT_STATUS;
			curr__DAT_DX		<= next__DAT_DX;
			curr__DAT_DY		<= next__DAT_DY;
			curr__DAT_DZ		<= next__DAT_DZ;
			curr__IRQ			<= next__IRQ;
			curr__PKT_OK		<= next__PKT_OK;
		end
	end

	/* next state ctrl */
	always @( * ) begin
		/* default values */
		next__state				= curr__state;
		next__count				= curr__count;
		next__TX_XMIT			= `FALSE;
		next__TX_BYTE			= curr__TX_BYTE;
		next__RX_RECV_EN		= `FALSE;
		next__DAT_STATUS		= curr__DAT_STATUS;
		next__DAT_DX			= curr__DAT_DX;
		next__DAT_DY			= curr__DAT_DY;
		next__DAT_DZ			= curr__DAT_DZ;
		next__IRQ				= curr__IRQ;
		next__PKT_OK			= `FALSE;

		case( curr__state )
			/* initial state - wait for 10ms before resetting the mouse */
			`STATE__WAIT_RESET: begin
				if( curr__count == 5_000_000 ) begin /* 10ms @50MHz */
					next__state			= `STATE__RESET;
					next__count			= 24'h000000;
				end
				else begin
					next__count			= curr__count + 1;
				end
			end

			/* start init by sending 0xFF */
			`STATE__RESET: begin
				next__state				= `STATE__WAIT_RESET_XMIT;
				next__TX_XMIT			= `TRUE;
				next__TX_BYTE			= `CMD__RESET;
			end

			/* wait for confirmation that the byte has been sent */
			`STATE__WAIT_RESET_XMIT: begin
				if( TX_XMIT_OK ) begin
					next__state			= `STATE__WAIT_RESET_ACK;
				end
			end

			/* should recv 0xFA, if not, go back to reset */
			`STATE__WAIT_RESET_ACK: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__ACK ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__WAIT_BAT_ACK;
					end
					else begin
						next__state		= `STATE__WAIT_RESET;
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* wait for self-test pass; if error, reset */
			`STATE__WAIT_BAT_ACK: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__BAT_PASS ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__WAIT_MOUSE_ID;
					end
					else begin
						next__state		= `STATE__WAIT_RESET;
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* wait for mouse ID byte. if error, reset */
			`STATE__WAIT_MOUSE_ID: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__MOUSE_ID_BASIC ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						/* find out if we have a scrollwheel */
						if( EN_SCROLL )
							next__state		= `STATE__SCROLL_SSR_0;
						else
							next__state		= `STATE__EN_BASIC;
					end
					else begin
						next__state		= `STATE__WAIT_RESET;
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/*------------------------------------------------------------------
			 * BASIC MODE REPORTING
			 *----------------------------------------------------------------*/
			/* send 0xF4 (enable reporting) */
			`STATE__EN_BASIC: begin
				next__state				= `STATE__EN_BASIC_XMIT;
				next__TX_XMIT			= `TRUE;
				next__TX_BYTE			= `CMD__ENABLE;
			end

			/* wait for confirmation of byte sent */
			`STATE__EN_BASIC_XMIT: begin
				if( TX_XMIT_OK ) begin
					next__state			= `STATE__EN_BASIC_ACK;
`ifdef PS2_DEBUG
					next__TX_BYTE	= 8'h00;
`endif
				end
			end

			/* if recv 0xFA, continue, else reset */
			`STATE__EN_BASIC_ACK: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__ACK ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__BASIC_0;
					end
					else begin
						next__state		= `STATE__WAIT_RESET;
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* INIT SUCCESSFUL: Start reading data */
			/* wait for status byte */
			`STATE__BASIC_0: begin
				if( RX_RECV_OK ) begin
					/* check for error in packet transmission */
					if( RX_BYTE_ERR != 2'b00 ) begin
						next__count		= curr__count + 1;
					end
					next__state			= `STATE__BASIC_1;
					next__DAT_STATUS	= RX_BYTE;
				end
				/* drop the IRQ line when the ACK has arrived */
				if( ACK )
					next__IRQ			= `FALSE;
				next__count				= 24'h000000;
				next__RX_RECV_EN		= `TRUE;
			end

			/* wait for DX byte */
			`STATE__BASIC_1: begin
				if( RX_RECV_OK ) begin
					if( RX_BYTE_ERR != 2'b00 ) begin
						next__count		= curr__count + 1;
					end
					next__state			= `STATE__BASIC_2;
					next__DAT_DX		= RX_BYTE;
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* wait for DY byte */
			`STATE__BASIC_2: begin
				if( RX_RECV_OK ) begin
					if( RX_BYTE_ERR != 2'b00 ) begin
						next__count		= curr__count + 1;
					end
					next__state			= `STATE__BASIC_IRQ;
					next__DAT_DY		= RX_BYTE;
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* IRQ time */
			`STATE__BASIC_IRQ: begin
				next__state				= `STATE__BASIC_0;
				/* only send an IRQ if the packet was intact */
				if( curr__count == 24'h000000 ) begin
					next__IRQ			= `TRUE;
					next__PKT_OK		= `TRUE;
				end
				next__count				= 24'h000000;
			end

			/*------------------------------------------------------------------
			 * SCROLL MODE SETUP
			 *----------------------------------------------------------------*/
			`STATE__SCROLL_SSR_0: begin
				next__state				= `STATE__SCROLL_SSR_0_XMIT;
				next__TX_XMIT			= `TRUE;
				next__TX_BYTE			= `CMD__SET_SAMPLE_RATE;
			end

			`STATE__SCROLL_SSR_0_XMIT: begin
				if( TX_XMIT_OK ) begin
					next__state			= `STATE__SCROLL_SSR_0_ACK;
`ifdef PS2_DEBUG
					next__TX_BYTE		= 8'h00;
`endif
				end
			end

			`STATE__SCROLL_SSR_0_ACK: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__ACK ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__SCROLL_SSR_0_VAL;
					end
					else begin
						next__state		= `STATE__WAIT_RESET; /* retry */
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			`STATE__SCROLL_SSR_0_VAL: begin
				next__state				= `STATE__SCROLL_SSR_0_VAL_XMIT;
				next__TX_XMIT			= `TRUE;
				next__TX_BYTE			= 8'hC8; /* 200 */
			end

			`STATE__SCROLL_SSR_0_VAL_XMIT: begin
				if( TX_XMIT_OK ) begin
					next__state			= `STATE__SCROLL_SSR_0_VAL_ACK;
`ifdef PS2_DEBUG
					next__TX_BYTE		= 8'h00;
`endif
				end
			end

			`STATE__SCROLL_SSR_0_VAL_ACK: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__ACK ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__SCROLL_SSR_1;
					end
					else begin
						next__state		= `STATE__WAIT_RESET; /* retry */
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			`STATE__SCROLL_SSR_1: begin
				next__state				= `STATE__SCROLL_SSR_1_XMIT;
				next__TX_XMIT			= `TRUE;
				next__TX_BYTE			= `CMD__SET_SAMPLE_RATE;
			end

			`STATE__SCROLL_SSR_1_XMIT: begin
				if( TX_XMIT_OK ) begin
					next__state			= `STATE__SCROLL_SSR_1_ACK;
`ifdef PS2_DEBUG
					next__TX_BYTE		= 8'h00;
`endif
				end
			end

			`STATE__SCROLL_SSR_1_ACK: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__ACK ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__SCROLL_SSR_1_VAL;
					end
					else begin
						next__state		= `STATE__WAIT_RESET; /* retry */
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			`STATE__SCROLL_SSR_1_VAL: begin
				next__state				= `STATE__SCROLL_SSR_1_VAL_XMIT;
				next__TX_XMIT			= `TRUE;
				next__TX_BYTE			= 8'h64; /* 100 */
			end

			`STATE__SCROLL_SSR_1_VAL_XMIT: begin
				if( TX_XMIT_OK ) begin
					next__state			= `STATE__SCROLL_SSR_1_VAL_ACK;
`ifdef PS2_DEBUG
					next__TX_BYTE		= 8'h00;
`endif
				end
			end

			`STATE__SCROLL_SSR_1_VAL_ACK: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__ACK ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__SCROLL_SSR_2;
					end
					else begin
						next__state		= `STATE__WAIT_RESET; /* retry */
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			`STATE__SCROLL_SSR_2: begin
				next__state				= `STATE__SCROLL_SSR_2_XMIT;
				next__TX_XMIT			= `TRUE;
				next__TX_BYTE			= `CMD__SET_SAMPLE_RATE;
			end

			`STATE__SCROLL_SSR_2_XMIT: begin
				if( TX_XMIT_OK ) begin
					next__state			= `STATE__SCROLL_SSR_2_ACK;
`ifdef PS2_DEBUG
					next__TX_BYTE		= 8'h00;
`endif
				end
			end

			`STATE__SCROLL_SSR_2_ACK: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__ACK ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__SCROLL_SSR_2_VAL;
					end
					else begin
						next__state		= `STATE__WAIT_RESET; /* retry */
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			`STATE__SCROLL_SSR_2_VAL: begin
				next__state				= `STATE__SCROLL_SSR_2_VAL_XMIT;
				next__TX_XMIT			= `TRUE;
				next__TX_BYTE			= 8'h50; /* 80 */
			end

			`STATE__SCROLL_SSR_2_VAL_XMIT: begin
				if( TX_XMIT_OK ) begin
					next__state			= `STATE__SCROLL_SSR_2_VAL_ACK;
`ifdef PS2_DEBUG
					next__TX_BYTE		= 8'h00;
`endif
				end
			end

			`STATE__SCROLL_SSR_2_VAL_ACK: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__ACK ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__SCROLL_RDT;
					end
					else begin
						next__state		= `STATE__WAIT_RESET; /* retry */
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			`STATE__SCROLL_RDT: begin
				next__state				= `STATE__SCROLL_RDT_XMIT;
				next__TX_XMIT			= `TRUE;
				next__TX_BYTE			= `CMD__READ_DEVICE_TYPE;
			end

			`STATE__SCROLL_RDT_XMIT: begin
				if( TX_XMIT_OK ) begin
					next__state			= `STATE__SCROLL_RDT_ACK;
`ifdef PS2_DEBUG
					next__TX_BYTE		= 8'h00;
`endif
				end
			end

			`STATE__SCROLL_RDT_ACK: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__ACK ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__SCROLL_RDT_VAL;
					end
					else begin
						next__state		= `STATE__WAIT_RESET; /* retry */
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			`STATE__SCROLL_RDT_VAL: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__MOUSE_ID_SCROLL ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__EN_SCROLL;
					end
					else if( ( RX_BYTE == `ANS__MOUSE_ID_BASIC ) &
							 ( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__EN_BASIC;
					end
					else begin
						next__state		= `STATE__WAIT_RESET; /* retry */
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/*------------------------------------------------------------------
			 * SCROLL MODE REPORTING ENABLE
			 *----------------------------------------------------------------*/
			 /* send 0xF4 (enable reporting) */
			`STATE__EN_SCROLL: begin
				next__state				= `STATE__EN_SCROLL_XMIT;
				next__TX_XMIT			= `TRUE;
				next__TX_BYTE			= `CMD__ENABLE;
			end

			/* wait for confirmation of byte sent */
			`STATE__EN_SCROLL_XMIT: begin
				if( TX_XMIT_OK ) begin
					next__state			= `STATE__EN_SCROLL_ACK;
`ifdef PS2_DEBUG
					next__TX_BYTE		= 8'h00;
`endif
				end
			end

			/* if recv 0xFA, continue, else reset */
			`STATE__EN_SCROLL_ACK: begin
				if( RX_RECV_OK ) begin
					if( ( RX_BYTE == `ANS__ACK ) &
						( RX_BYTE_ERR == 2'b00 ) ) begin
						next__state		= `STATE__SCROLL_0;
					end
					else begin
						next__state		= `STATE__WAIT_RESET;
					end
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/*------------------------------------------------------------------
			 * SCROLL MODE REPORTING
			 *----------------------------------------------------------------*/
			/* INIT SUCCESSFUL: Start reading data */
			/* wait for status byte */
			`STATE__SCROLL_0: begin
				if( RX_RECV_OK ) begin
					/* check for error in packet transmission */
					if( RX_BYTE_ERR != 2'b00 ) begin
						next__count		= curr__count + 1;
					end
					next__state			= `STATE__SCROLL_1;
					next__DAT_STATUS	= RX_BYTE;
				end
				/* drop the IRQ line when the ACK has arrived */
				if( ACK )
					next__IRQ			= `FALSE;
				next__count				= 24'h000000;
				next__RX_RECV_EN		= `TRUE;
			end

			/* wait for DX byte */
			`STATE__SCROLL_1: begin
				if( RX_RECV_OK ) begin
					if( RX_BYTE_ERR != 2'b00 ) begin
						next__count		= curr__count + 1;
					end
					next__state			= `STATE__SCROLL_2;
					next__DAT_DX		= RX_BYTE;
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* wait for DY byte */
			`STATE__SCROLL_2: begin
				if( RX_RECV_OK ) begin
					if( RX_BYTE_ERR != 2'b00 ) begin
						next__count		= curr__count + 1;
					end
					next__state			= `STATE__SCROLL_3;
					next__DAT_DY		= RX_BYTE;
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* wait for DZ byte - it only really uses the low 4 bits  */
			`STATE__SCROLL_3: begin
				if( RX_RECV_OK ) begin
					if( RX_BYTE_ERR != 2'b00 ) begin
						next__count		= curr__count + 1;
					end
					next__state			= `STATE__SCROLL_IRQ;
					next__DAT_DZ		= RX_BYTE[3:0];
				end
				next__RX_RECV_EN		= `TRUE;
			end

			/* IRQ time */
			`STATE__SCROLL_IRQ: begin
				next__state				= `STATE__SCROLL_0;
				/* only send an IRQ if the packet was intact */
				if( curr__count == 24'h000000 ) begin
					next__IRQ			= `TRUE;
					next__PKT_OK		= `TRUE;
				end
				next__count				= 24'h000000;
			end

			/* default state - should not be reachable */
			default: begin
				next__state				= 4'h0;
				next__count				= 24'h000000;
				next__TX_XMIT			= `FALSE;
				next__TX_BYTE			= 8'hFF;
				next__RX_RECV_EN		= `FALSE;
				next__DAT_STATUS		= 8'h00;
				next__DAT_DX			= 8'h00;
				next__DAT_DY			= 8'h00;
				next__DAT_DZ			= 4'h0;
				next__IRQ				= `FALSE;
			end
		endcase
	end

	/* assign signals to ports */
	/* Tx side */
	assign TX_XMIT						= curr__TX_XMIT;
	assign TX_BYTE						= curr__TX_BYTE;
	/* Rx side */
	assign RX_RECV_EN					= curr__RX_RECV_EN;
	/* outputs */
	assign DAT_STATUS					= curr__DAT_STATUS;
	assign DAT_DX						= curr__DAT_DX;
	assign DAT_DY						= curr__DAT_DY;
	assign DAT_DZ						= curr__DAT_DZ;
	assign IRQ							= curr__IRQ;
	assign PKT_OK						= curr__PKT_OK;

`ifdef PS2_DEBUG
	assign DBG_STATE					= curr__state;
`endif

endmodule
