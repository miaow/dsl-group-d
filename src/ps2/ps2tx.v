`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
//
// Create Date:		09:18:33 01/23/2014
// Design Name:		PS/2 Demo
// Module Name:		ps2tx
// Project Name:	PS/2 Demo
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		The PS/2 transmitter module.
//
// Dependencies:	globals.v
//
// Revision:
// v1.00 - 201402080030Z - Verified
// v0.02 - 201401251920Z - Implementation done
// v0.01 - 201401230918Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////
`include "../globals.v"
`define TX__STATE_IDLE					4'h0
`define TX__STATE_XMIT_BUS_INHIBIT		4'h1
`define TX__STATE_REQUEST_TO_SEND		4'h2
`define TX__STATE_START					4'h3
`define TX__STATE_SEND_BYTE				4'h4
`define TX__STATE_SEND_PARITY			4'h5
`define TX__STATE_SEND_STOP_BIT			4'h6
`define TX__STATE_WAIT_ACK_DATA			4'h7
`define TX__STATE_WAIT_ACK_CLK			4'h8
`define TX__STATE_WAIT_BUS_FREE			4'h9
`define TX__STATE_DEFAULT				default

module ps2tx
(
    input			RESET,
    input			CLK,
    input			IO_CLK_IN,
    output			IO_CLK_OE,
    input			IO_DAT_IN,
    output			IO_DAT_OE,
    output			IO_DAT_OUT,
    input			XMIT,
    input	[7:0]	BYTE,
    output			XMIT_OK
);

	/* buffer the i/o clock in order to detect clock edges */
	reg buf__IO_CLK_IN;
	always @(posedge CLK)
		buf__IO_CLK_IN						<= IO_CLK_IN;


	/* state machine controlling the sending of data */
	reg		[3:0]	curr__state,			next__state;
	reg				curr__IO_CLK_OE,		next__IO_CLK_OE;
	reg				curr__IO_DAT_OUT,		next__IO_DAT_OUT;
	reg				curr__IO_DAT_OE,		next__IO_DAT_OE;
	reg		[15:0]	curr__send_count,		next__send_count;
	reg				curr__XMIT_OK,			next__XMIT_OK;
	reg		[7:0]	curr__BYTE,				next__BYTE;

	/* update the state at each clock tick */
	always @( posedge CLK or posedge RESET ) begin
		if(RESET) begin
			curr__state						<= `TX__STATE_IDLE;
			curr__IO_CLK_OE					<= `FALSE;
			curr__IO_DAT_OE					<= `FALSE;
			curr__IO_DAT_OUT				<= `FALSE;
			curr__send_count				<= 16'h0000;
			curr__XMIT_OK					<= `FALSE;
			curr__BYTE						<= 8'h00;
		end
		else begin
			curr__state						<= next__state;
			curr__IO_CLK_OE					<= next__IO_CLK_OE;
			curr__IO_DAT_OE					<= next__IO_DAT_OE;
			curr__IO_DAT_OUT				<= next__IO_DAT_OUT;
			curr__send_count				<= next__send_count;
			curr__XMIT_OK					<= next__XMIT_OK;
			curr__BYTE						<= next__BYTE;
		end
	end

	/* combinatorial logic */
	always @( * ) begin
		/* set default values */
		next__state							= curr__state;
		next__IO_CLK_OE						= `FALSE;
		next__IO_DAT_OE						= curr__IO_DAT_OE;
		next__IO_DAT_OUT					= curr__IO_DAT_OUT;
		next__send_count					= curr__send_count;
		next__XMIT_OK						= `FALSE;
		next__BYTE							= curr__BYTE;

		case( curr__state )

			`TX__STATE_IDLE: begin
				if( XMIT ) begin
					next__state				= `TX__STATE_XMIT_BUS_INHIBIT;
					next__BYTE				= BYTE;
				end
				next__IO_DAT_OE				= `FALSE;
			end

			`TX__STATE_XMIT_BUS_INHIBIT: begin
				/* set clock line low for 100us, or 5000 ticks minimum @50MHz */
				if( curr__send_count == 6000 ) begin
					next__state				= `TX__STATE_REQUEST_TO_SEND;
					next__send_count		= 16'h0000;
				end
				else begin
					next__send_count		= curr__send_count + 1;
				end
				next__IO_CLK_OE 			= `TRUE;
			end

			`TX__STATE_REQUEST_TO_SEND: begin
				/* pull data line low and release clock line */
				next__state					= `TX__STATE_START;
				next__IO_DAT_OE				= `TRUE;
				next__IO_DAT_OUT			= `LOW;
				next__IO_CLK_OE 			= `FALSE;
			end

			`TX__STATE_START: begin
				/* start bit = 0, already set, just wait for clock edge */
				if( buf__IO_CLK_IN & ~IO_CLK_IN )
					next__state				= `TX__STATE_SEND_BYTE;
			end

			`TX__STATE_SEND_BYTE: begin
				/* send bits 0-7, LSB first */
				/* change data at the falling edge of the clock to obey timing
				 * restrictions */
				if( buf__IO_CLK_IN & ~IO_CLK_IN ) begin
					if( curr__send_count == 16'h0007 ) begin
						next__state			= `TX__STATE_SEND_PARITY;
						next__send_count	= 16'h0000;
					end
					else
						next__send_count	= curr__send_count + 16'h0001;
				end

				next__IO_DAT_OUT			= curr__BYTE[curr__send_count];
			end

			`TX__STATE_SEND_PARITY: begin
				/* change data at the falling edge of the clock to obey timing
				 * restrictions */
				if( buf__IO_CLK_IN & ~IO_CLK_IN )
					next__state				= `TX__STATE_SEND_STOP_BIT;

				next__IO_DAT_OUT			= ~^curr__BYTE[7:0];
			end

			`TX__STATE_SEND_STOP_BIT: begin
				/* releasing the data line will pull it high (open collector
				 * feature) -> the stop bit */
				if( buf__IO_CLK_IN & ~IO_CLK_IN )
					next__state					= `TX__STATE_WAIT_ACK_DATA;
				next__IO_DAT_OUT			= `HIGH;
				next__IO_DAT_OE				= `FALSE;
			end

			/* device acknowledge procedure:
			 *     - pull data line low
			 *     - pull clock line low
			 *     - release both clock and data lines
			 */
			`TX__STATE_WAIT_ACK_DATA: begin
				if( ~IO_DAT_IN )
					next__state				= `TX__STATE_WAIT_ACK_CLK;
			end

			`TX__STATE_WAIT_ACK_CLK: begin
				if( ~IO_CLK_IN )
					next__state				= `TX__STATE_WAIT_BUS_FREE;
			end

			`TX__STATE_WAIT_BUS_FREE: begin
				if( IO_DAT_IN & IO_CLK_IN ) begin
					next__state				= `TX__STATE_IDLE;
					next__XMIT_OK			= `TRUE;
`ifdef PS2_DEBUG
					next__BYTE				= 8'h00;
`endif
				end
			end

			`TX__STATE_DEFAULT: begin
				/* default state - should not be reachable */
				next__state					= `TX__STATE_IDLE;
				next__IO_CLK_OE				= `FALSE;
				next__IO_DAT_OE				= `FALSE;
				next__IO_DAT_OUT			= `HIGH;
				next__send_count			= 16'h0000;
				next__XMIT_OK				= `FALSE;
				next__BYTE					= 8'h00;
			end

		endcase
	end

	/* assign outputs */
	/* clock */
	assign IO_CLK_OE						= curr__IO_CLK_OE;
	/* data */
	assign IO_DAT_OE						= curr__IO_DAT_OE;
	assign IO_DAT_OUT						= curr__IO_DAT_OUT;
	/* to controller */
	assign XMIT_OK							= curr__XMIT_OK;

endmodule
