`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
//
// Create Date:		10:08:45 01/30/2014
// Design Name:		PS/2 Demo
// Module Name:		bus_ps2
// Project Name:	PS/2 Demo
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		The PS/2 module - complete, bus-wrapped.
//
// Dependencies:	globals.v, ps2rx.v, ps2tx.v
//
// Revision:
// v1.00 - 201402081909Z - Verified
// v0.02 - 201401301100Z - Implementation done
// v0.01 - 201401301008Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////
`include "../globals.v"
`define PS2_ADDR_0 8'hA0
`define PS2_ADDR_1 8'hA1
`define PS2_ADDR_2 8'hA2
/* PS2_ADDR_0 outputs either the status byte or the dz byte, depending on the
 * state set by writing to the same address.
 * 0: output status byte
 * 1: output dz byte
 */


module bus_ps2
(
`ifdef PS2_DEBUG
	output		[7:0]	DBG_TX_BYTE,
	output		[7:0]	DBG_RX_BYTE,
	output		[5:0]	DBG_STATE,
	output		[1:0]	DBG_RX_ERR,
`endif
	input				CLK,
	input				RESET,
	/* bus lines */
	inout		[7:0]	BUS_DATA,
	input		[7:0]	BUS_ADDR,
	input				BUS_DATA_OE,
	output				BUS_IRQ,
	input				BUS_ACK,
	/* I/O */
	inout				IO_CLK,
	inout				IO_DAT
);

	/* state of the PS2_ADDR_0 output. Change it if the MCU writes to
	 * PS2_ADDR_0. Otherwise leave it as is.
	 * State 0: output status byte @mouse_0
	 * State 1: output z byte @mouse_0
	 * State 2: assert software reset for controller
	 */
	reg			[1:0]	PS2_ADDR_0_state;
	always@( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			PS2_ADDR_0_state		<= 2'b00;
		end
		else begin
			if( BUS_DATA_OE & BUS_ADDR == `PS2_ADDR_0 ) begin
				PS2_ADDR_0_state	<= BUS_DATA[1:0];
			end
			else begin
				PS2_ADDR_0_state	<= PS2_ADDR_0_state;
			end
		end
	end

	/* software reset for the controller */
	wire sw_reset;
	assign sw_reset = PS2_ADDR_0_state == 2'b10;

	/* scroll enable register. set when sw_reset is asserted. to do so, write
	 * to @mouse_1
	 */
	reg					en_scroll;
	always@( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			en_scroll		<= `FALSE;
		end
		else begin
			if( BUS_ADDR == `PS2_ADDR_0 &
				BUS_DATA_OE &
				PS2_ADDR_0_state == 2'b10 ) begin
				en_scroll	<= BUS_DATA[0];
			end
			else begin
				en_scroll	<= en_scroll;
			end
		end
	end

	/* bus arbitration */
	reg					BUS_PS2_OE;
	wire		[7:0]	BUS_DATA_IN;
	reg			[7:0]	BUS_DATA_OUT;
	assign BUS_DATA_IN = BUS_DATA;
	assign BUS_DATA = BUS_PS2_OE ? BUS_DATA_OUT : 8'hzz;

	/* only enable the output if the MCU wants to read from our addresses */
	always@( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			BUS_PS2_OE			<= `FALSE;
		end
		else begin
			if( ~BUS_DATA_OE &
				( BUS_ADDR == `PS2_ADDR_0 |
				  BUS_ADDR == `PS2_ADDR_1 |
				  BUS_ADDR == `PS2_ADDR_2 ) )
				BUS_PS2_OE		<= `TRUE;
			else
				BUS_PS2_OE		<= `FALSE;
		end
	end

	/* Mouse position limits - 160x120 screen */
	parameter	[7:0]	DAT_X_POS_max	= 160;
	parameter	[7:0]	DAT_Y_POS_max	= 120;

	/* Tri-state signals (IO_DAT and IO_CLK) */
	/* IO_CLK */
	reg					IO_CLK_IN;
	wire				IO_CLK_OE;
	/* if output enabled, pull IO_CLK low */
	assign IO_CLK						= IO_CLK_OE ? `LOW : `Z;

	/* IO_DAT */
	wire				IO_DAT_IN;
	wire				IO_DAT_OUT;
	wire				IO_DAT_OE;
	/* if output enabled, drive data line, else leave at high impedance */
	assign IO_DAT						= IO_DAT_OE ? IO_DAT_OUT : `Z;
	assign IO_DAT_IN					= IO_DAT;

	/* clock filter - ensures clock is stable before latching data */
	reg	[7:0]		buf_IO_CLK;
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			IO_CLK_IN		<= `LOW;
		end
		else begin
			/* simple shift reg */
			buf_IO_CLK[7:1]	<= buf_IO_CLK[6:0];
			buf_IO_CLK[0]	<= IO_CLK;

			/* detect falling edge */
			if( IO_CLK_IN & ( buf_IO_CLK == 8'h00 ) ) begin
				IO_CLK_IN	<= `LOW;
			end
			/* and rising edge */
			if( ~IO_CLK_IN & ( buf_IO_CLK == 8'hFF ) ) begin
				IO_CLK_IN	<= `HIGH;
			end
		end
	end

	/* generate a Tx module instance */
	wire			TX_XMIT;
	wire			TX_XMIT_OK;
	wire	[7:0]	TX_BYTE;
	ps2tx tx
	(
		.CLK(CLK),
		.RESET(RESET),
		/* I/O - CLK */
		.IO_CLK_IN(IO_CLK_IN),
		.IO_CLK_OE(IO_CLK_OE),
		/* I/O - DATA */
		.IO_DAT_IN(IO_DAT_IN),
		.IO_DAT_OUT(IO_DAT_OUT),
		.IO_DAT_OE(IO_DAT_OE),
		/* ctrl */
		.XMIT(TX_XMIT),
		.BYTE(TX_BYTE),
		.XMIT_OK(TX_XMIT_OK)
	);

	/* and an Rx module instance */
	wire			RX_RECV_EN;
	wire	[7:0]	RX_BYTE;
	wire	[1:0]	RX_BYTE_ERR;
	wire			RX_RECV_OK;
	ps2rx rx
	(
		.CLK(CLK),
		.RESET(RESET),
		/* I/O - CLK */
		.IO_CLK_IN(IO_CLK_IN),
		/* I/I - DATA */
		.IO_DAT_IN(IO_DAT_IN),
		/* ctrl */
		.RECV_EN(RX_RECV_EN),
		.BYTE(RX_BYTE),
		.BYTE_ERR(RX_BYTE_ERR),
		.RECV_OK(RX_RECV_OK)
	);

	/* and the state machine */
	wire	[7:0]	DAT_STATUS_raw;
	wire	[7:0]	DAT_DX_raw;
	wire	[7:0]	DAT_DY_raw;
	wire	[3:0]	DAT_DZ_raw;
	wire			IRQ;
	wire			PKT_OK;
	assign BUS_IRQ = IRQ;
// 	wire			ACK;
	ps2ctrl ctrl
	(
`ifdef PS2_DEBUG
		.DBG_STATE(DBG_STATE),
`endif
		.CLK(CLK),
		.RESET(RESET | sw_reset),
		.EN_SCROLL(en_scroll),
		/* Tx interface */
		.TX_XMIT(TX_XMIT),
		.TX_BYTE(TX_BYTE),
		.TX_XMIT_OK(TX_XMIT_OK),
		/* Rx interface */
		.RX_RECV_EN(RX_RECV_EN),
		.RX_BYTE(RX_BYTE),
		.RX_BYTE_ERR(RX_BYTE_ERR),
		.RX_RECV_OK(RX_RECV_OK),
		/* data */
		.DAT_STATUS(DAT_STATUS_raw),
		.DAT_DX(DAT_DX_raw),
		.DAT_DY(DAT_DY_raw),
		.DAT_DZ(DAT_DZ_raw),
		.PKT_OK(PKT_OK),
		.IRQ(IRQ),
		.ACK(BUS_ACK)
	);

	/* clamp the dx and dy values in hardware as this is extremely expensive
	 * to do in software
	 */
	wire signed	[8:0]	DAT_DX;
	wire signed	[8:0]	DAT_DY;
	wire 		[3:0]	DAT_DZ;
	wire signed	[8:0]	next__DAT_X_POS;
	wire signed	[8:0]	next__DAT_Y_POS;
	wire signed	[8:0]	next__DAT_Z_POS;
	reg			[3:0]	DAT_STATUS;
	reg			[7:0]	DAT_X_POS;
	reg			[6:0]	DAT_Y_POS;
	reg			[3:0]	DAT_Z_POS;
// 	wire signed	[8:0]	DAT_DY_inv;

	assign DAT_DX = DAT_STATUS_raw[6] ?
		( DAT_STATUS_raw[4] ?
			{DAT_STATUS_raw[4],8'h00} :
			{DAT_STATUS_raw[4],8'hFF} ) :
		{DAT_STATUS_raw[4],DAT_DX_raw[7:0]};

	assign DAT_DY = DAT_STATUS_raw[7] ?
		( DAT_STATUS_raw[5] ?
			{DAT_STATUS_raw[5],8'h00} :
			{DAT_STATUS_raw[5],8'hFF} ) :
		{DAT_STATUS_raw[5],DAT_DY_raw[7:0]};

	assign DAT_DZ = DAT_DZ_raw;

	/* calculate the mouse position */
	assign next__DAT_X_POS = {1'b0, DAT_X_POS} + DAT_DX;
	/* reverse the movement for Dy to make the directions consistent with VGA
	 * addressing mode.
	 */
	assign next__DAT_Y_POS = {2'b00, DAT_Y_POS} - DAT_DY;

	assign next__DAT_Z_POS = DAT_Z_POS + DAT_DZ;

	/* update the o/p registers */
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			DAT_STATUS			<= 4'h0;
			DAT_X_POS			<= DAT_X_POS_max/2;
			DAT_Y_POS			<= DAT_Y_POS_max/2;
			DAT_Z_POS			<= 4'h0;
		end
		else if( PKT_OK ) begin
			/* strip the status of the redundant info */
			DAT_STATUS			<= DAT_STATUS_raw[3:0];

			/* modify X position wrt screen limits */
			if( next__DAT_X_POS < 0 )
				DAT_X_POS		<= 0;
			else if( next__DAT_X_POS > ( DAT_X_POS_max - 1 ) )
				DAT_X_POS		<= DAT_X_POS_max - 1;
			else
				DAT_X_POS		<= next__DAT_X_POS[7:0];

			/* modify Y position wrt screen limits */
			if( next__DAT_Y_POS < 0 )
				DAT_Y_POS		<= 0;
			else if( next__DAT_Y_POS > ( DAT_Y_POS_max - 1 ) )
				DAT_Y_POS		<= DAT_Y_POS_max - 1;
			else
				DAT_Y_POS		<= next__DAT_Y_POS[6:0];

			/* modify Z position */
			DAT_Z_POS			<= next__DAT_Z_POS;
		end
	end

	/* output logic */
	always@( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			BUS_DATA_OUT		<= 8'h00;
		end
		else begin
			if( BUS_ADDR == `PS2_ADDR_0 & ~PS2_ADDR_0_state)
				BUS_DATA_OUT	<= DAT_STATUS;
			else if( BUS_ADDR == `PS2_ADDR_0 & PS2_ADDR_0_state )
				BUS_DATA_OUT	<= DAT_Z_POS;
			else if( BUS_ADDR == `PS2_ADDR_1 )
				BUS_DATA_OUT	<= DAT_X_POS;
			else if( BUS_ADDR == `PS2_ADDR_2 )
				BUS_DATA_OUT	<= DAT_Y_POS;
			else
				BUS_DATA_OUT	<= 8'h00;
		end
	end

`ifdef PS2_DEBUG
	/* assign debug outputs */
	assign DBG_TX_BYTE			= TX_BYTE;
	assign DBG_RX_BYTE			= RX_BYTE;
	assign DBG_RX_ERR			= RX_BYTE_ERR;
`endif

endmodule
