`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
//
// Create Date:		09:18:33 01/23/2014
// Design Name:		PS/2 Demo
// Module Name:		bus_s7dec
// Project Name:	PS/2 Demo
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		The 7-segment display bus interface.
//
// Dependencies:	globals.v
//
// Revision:
// v1.00 - 201402081904Z - Verified
// v0.02 - 201401251920Z - Implementation done
// v0.01 - 201401230918Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////
`include "../globals.v"

`define LED_ADDR 8'hC0

module bus_leds
(
	input			CLK,
	input			RESET,
	/* bus signals */
	input	[7:0]	BUS_DATA,
	input	[7:0]	BUS_ADDR,
	/* passthrough for the physical outputs */
	output	[7:0]	LED_OUT
);

	reg		[7:0]	status;
// 	reg		[3:0]	next__digits [0:3];
	
	/* sequential logic - simply update the register */
	always@( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			status					<= 8'h00;
		end
		else begin
			if( BUS_ADDR == `LED_ADDR ) begin
				status				<= BUS_DATA;
			end
			else begin
				status				<= status;
			end
		end
	end
	
	/* assign the register value to the LED output port */
	assign LED_OUT = status;
	

endmodule