`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
//
// Create Date:		09:18:33 01/23/2014
// Design Name:		PS/2 Demo
// Module Name:		bus_s7dec
// Project Name:	PS/2 Demo
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		The 7-segment display bus interface.
//
// Dependencies:	globals.v
//
// Revision:
// v1.00 - 201402081904Z - Verified
// v0.02 - 201401251920Z - Implementation done
// v0.01 - 201401230918Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////
`include "../globals.v"

`define S7D_ADDR_X 8'hd0
`define S7D_ADDR_Y 8'hd1

module bus_s7dec
(
	input			CLK,
	input			RESET,
	/* bus signals */
	input	[7:0]	BUS_DATA,
	input	[7:0]	BUS_ADDR,
	/* passthrough for the physical outputs */
	output	[3:0]	SEG_SELECT,
	output	[7:0]	HEX_OUT
);

	reg		[3:0]	curr__digits [3:0];
// 	reg		[3:0]	next__digits [0:3];
	
	/* sequential logic - simply update the registers */
	always@( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			curr__digits[3]			<= 4'h5;
			curr__digits[2]			<= 4'h0;
			curr__digits[1]			<= 4'h3;
			curr__digits[0]			<= 4'hc;
		end
		else begin
			if( BUS_ADDR == `S7D_ADDR_X ) begin
				curr__digits[3]		<= BUS_DATA[7:4];
				curr__digits[2]		<= BUS_DATA[3:0];
			end
			else begin
				curr__digits[3]		<= curr__digits[3];
				curr__digits[2]		<= curr__digits[2];
			end
			
			if( BUS_ADDR == `S7D_ADDR_Y ) begin
				curr__digits[1]		<= BUS_DATA[7:4];
				curr__digits[0]		<= BUS_DATA[3:0];
			end
			else begin
				curr__digits[1]		<= curr__digits[1];
				curr__digits[0]		<= curr__digits[0];
			end
		end
	end
	
	/* instantiate the 7-segment decoder module, and its corresponding driver
	 * counters
	 */
	wire	[1:0]	Bit2Count;
	wire			Bit16TrigOut;
	wire	[4:0]	MuxOut;
/*----------------------------------------------------------------------------*\
 *	16-bit Counter : Trigger pulse every 1ms (@1kHz)						  *
 *		CLK:		Clock input. 50MHz oscillator on M6 used.				  *
 *		RESET:		Reset line. Always deasserted.							  *
 *		ENABLE_IN:	Enable signal. External control from switch.			  *
 *		TRIG_OUT:	Trigger for both 2-bit counter and 	 					  *
 *					first 4-bit counter.									  *
 *		COUNT:		Unused.													  *
\*----------------------------------------------------------------------------*/
	generic_counter
	#(
		.COUNTER_WIDTH(16),
		.COUNTER_MAX(49_999))
	Bit16
	(
		.CLK(CLK),
		.RESET(1'b0),
		.ENABLE_IN(1'b1),
		.TRIG_OUT(Bit16TrigOut),
		.COUNT(),
		.DIRECTION(1'b1)
	);
	
/*----------------------------------------------------------------------------*\
 *	2-bit Counter : Controls 4-way mux to output to 7-segment display		  *
 *		CLK:		Clock input. 50MHz oscillator on M6 used.				  *
 *		RESET:		Reset line. Always deasserted to avoid 					  *
 *					display glitches.										  *
 *		ENABLE_IN:	Enable line. Controlled from 16-bit counter.			  *
 *		TRIG_OUT:	Unused.													  *
 *		COUNT:		Controls the mux and 7-segment decoder					  *
\*----------------------------------------------------------------------------*/
	generic_counter
	#(
		.COUNTER_WIDTH(2),
		.COUNTER_MAX(3))
	Bit2
	(
		.CLK(CLK),
		.RESET(1'b0),
		.ENABLE_IN(Bit16TrigOut),
		.TRIG_OUT(),
		.COUNT(Bit2Count),
		.DIRECTION(1'b1)
	);
	
/*----------------------------------------------------------------------------*\
 *	4-way Mux. See diagram in logbook										  *
 *		S:	Select line														  *
 *		A:	Most significant digit i/p										  *
 *		B:	Digit 3 i/p														  *
 *		C:	Digit 2 i/p														  *
 *		D:	Least Significant digit i/p										  *
 *		OUT:	Output to the 7-segment decoder								  *
\*----------------------------------------------------------------------------*/
	mux4x5	mux
	(
		.S(Bit2Count),
		.D({1'b0,curr__digits[0]}),
		.C({1'b0,curr__digits[1]}),
		.B({1'b1,curr__digits[2]}),
		.A({1'b0,curr__digits[3]}),
		.OUT(MuxOut)
	);

/*----------------------------------------------------------------------------*\
 *	7-segment decoder. Determines which of the four digits of the display	  *
 *		to operate and decodes BNN values to 7-segment signals.				  *
 *		SEG_SELECT_IN:	Select line for which of the digits to				  *
 *					operate													  *
 *		BIN_IN:		Input data in BNN format.								  *
 *		DOT_IN:		Input bit for the decimal point segment.				  *
 *		SEG_SELECT_OUT:	4-bit positional code; selects one of the 4 		  *
 *					anodes of the display									  *
 *		HEX_OUT:	Display control signals. Determines the					  *
 *					symbol to be outputted.									  *
\*----------------------------------------------------------------------------*/
	seg7decoder	s7dec
	(
		.SEG_SELECT_IN(Bit2Count),
		.BIN_IN(MuxOut[3:0]),
		.DOT_IN(MuxOut[4]),
		.SEG_SELECT_OUT(SEG_SELECT),
		.HEX_OUT(HEX_OUT)
	);

endmodule