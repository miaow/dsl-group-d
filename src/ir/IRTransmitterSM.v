`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company: School of Engineering, University of Edinburgh
// Engineer: Roy Kong
//
// Create Date:    17:08:07 01/23/2014
// Design Name: 	 IR Transmitter Driver
// Module Name:    IRTransmitterSM
// Project Name: 	 IR_Transmitter
// Target Devices: Xilinx Spartan3E-100 (xc3s100e-4cp132)
// Tool versions:
// Description: A top module for the IR transmitter driver design, generating
//				car frequencies, 10Hz trigger signal and state machines for each
//				coloured car, finally a multiplexer for selecting the car pulse
//				output using the four switches on the left with a global reset
//				switch at the right. Direction of the car is controlled by the
//				push buttons.
//
// Dependencies: IR transmitter circuit, BASYS 2 FPGA board, usb cable,
//				50MHz clock, a USB port for power source
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
////////////////////////////////////////////////////////////////////////////////
`include "../globals.v"
//`define SOFTWARE_TRIG

module IRTransmitterSM
(
	/* Standard Signals */
	input				CLK,			// on board 50MHz clock
	input				RESET,			// first switch from the right
	/* Bus I/O */
	input		[7:0]	BUS_DATA,		// data bus
	input		[7:0]	BUS_ADDR,		// address bus
	input				BUS_DATA_OE,	// write enable signal
	/* global I/O */
	output reg			IR_LED			// transmit IR signal from JA header
);

	parameter	[7:0]	ir_base_addr	= 8'h90; /* base address */

`ifdef SOFTWARE_TRIG
	reg					CLK_10HZ;
`else
	wire				CLK_10HZ;
`endif

	wire		[3:0]	COMMAND;
	reg			[3:0]	SELECT;
	assign COMMAND = BUS_DATA[3:0];

	reg			[3:0]	direction;
	/* take direction sent from mcu into GenericSM if address is matched */
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			direction <= 4'h0;
		end
		else begin
			if ( ( BUS_ADDR == ir_base_addr ) & BUS_DATA_OE) begin
				/* decode the direction given by the MCU */
				case (COMMAND)
					4'b0000:	direction	<= 4'b1010;
					4'b0001:	direction	<= 4'b0010;
					4'b0010:	direction	<= 4'b0110;
					4'b0100:	direction	<= 4'b1000;
					4'b0101:	direction	<= 4'b0000;
					4'b0110:	direction	<= 4'b0100;
					4'b1000:	direction	<= 4'b1001;
					4'b1001:	direction	<= 4'b0001;
					4'b1010:	direction	<= 4'b0101;
					default:	direction	<= 4'b0000;
				endcase
				SELECT						<= BUS_DATA[7:4];
`ifdef SOFTWARE_TRIG
				CLK_10HZ <= `TRUE;
`endif
			end
			else begin
`ifdef SOFTWARE_TRIG
				CLK_10HZ <= `FALSE;
`endif
				direction <= direction;
			end
		end
	end

	/* Generate the pulse signal here from the main clock running at 50MHz to
	 * generate the right frequency for the car in question e.g. 36KHz for BLUE
	 * and RED coded cars, 37.5kHz for GREEN coded car and 40kHz for YELLOW
	 * coded car and a simple state machine to generate the states of the packet
	 * i.e. Start, Gaps, Right Assert or De-Assert, Left Assert or De-Assert,
	 * Backward Assert or De-Assert, and Forward Assert or De-Assert.
	 */

	wire BluePacket;
	wire YellowPacket;
	wire GreenPacket;
	wire RedPacket;

`ifndef SOFTWARE_TRIG
	/* Instantiation for 10Hz frequency counter - only if using
	 * a hardware trigger
	 */
	generic_counter
	#(
		.COUNTER_WIDTH (23),
		.COUNTER_MAX (4999999)
	)
		Counter_10Hz
		(
			.CLK(CLK),
			.RESET(RESET),
			.ENABLE_IN(1'b1),
			.DIRECTION(1'b1),
			.TRIG_OUT(CLK_10HZ)
		);
`endif

	// Instantiation for blue car state machine
	GenericSM
	#(
		.CarFrequencyCountSize(694),
		.StartBurstSize (191),
		.CarSelectBurstSize (47),
		.GapSize (25),
		.AssertBurstSize (47),
		.DeAssertBurstSize (22)
	)
		BlueCarSM
		(
			.CLK(CLK),
			.RESET(RESET),
			.COMMAND(direction),
			.SEND_PACKET(CLK_10HZ),
			.PULSES(BluePacket)
		);

	// Instantiation for yellow car state machine
	GenericSM
	#(
		.CarFrequencyCountSize(624),
		.StartBurstSize (88),
		.CarSelectBurstSize (22),
		.GapSize (40),
		.AssertBurstSize (44),
		.DeAssertBurstSize (22)
	)
		YellowCarSM
		(
			.CLK(CLK),
			.RESET(RESET),
			.COMMAND(direction),
			.SEND_PACKET(CLK_10HZ),
			.PULSES(YellowPacket)
		);

	// Instantiation for green car state machine
	GenericSM
	#(
		.CarFrequencyCountSize(666),
		.StartBurstSize (88),
		.CarSelectBurstSize (44),
		.GapSize (40),
		.AssertBurstSize (44),
		.DeAssertBurstSize (22)
	)
		GreenCarSM
		(
			.CLK(CLK),
			.RESET(RESET),
			.COMMAND(direction),
			.SEND_PACKET(CLK_10HZ),
			.PULSES(GreenPacket)
		);

	// Instantiation for red car state machine
	GenericSM
	#(
		.CarFrequencyCountSize(694),
		.StartBurstSize (192),
		.CarSelectBurstSize (24),
		.GapSize (24),
		.AssertBurstSize (48),
		.DeAssertBurstSize (24)
	)
		RedCarSM
		(
			.CLK(CLK),
			.RESET(RESET),
			.COMMAND(direction),
			.SEND_PACKET(CLK_10HZ),
			.PULSES(RedPacket)
		);


	// Finally, tie the pulse generator with the packets to generate IR_LED
	// Different coloured cars are activated by switches using a case statement
	always @( * ) begin
		case( SELECT )
			4'b0001: 		IR_LED = BluePacket;
			4'b0010: 		IR_LED = YellowPacket;
			4'b0100: 		IR_LED = GreenPacket;
			4'b1000: 		IR_LED = RedPacket;
			default:		IR_LED = 0;
		endcase
	end

endmodule
