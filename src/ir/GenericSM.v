`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company: School of Engineering, University of Edinburgh
// Engineer: Roy Kong
//
// Create Date:    09:29:42 02/04/2014
// Design Name: 	IR Transmitter Driver
// Module Name:    GenericSM
// Project Name: 	IR_Transmitter
// Target Devices: Xilinx Spartan3E-100 (xc3s100e-4cp132)
// Tool versions:
// Description: A generic state machine for generating the car pulses which the
//					parameters are variable
//
// Dependencies: IR transmitter circuit, BASYS 2 FPGA board, usb cable, 50MHz
//					clock, a USB port for power source
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
////////////////////////////////////////////////////////////////////////////////
`include "../globals.v"

module GenericSM
(
	CLK,
	RESET,
	COMMAND,
	SEND_PACKET,
	PULSES
);

	/* define parameters for generating the necessary pulse codes for all of the
	 * variations required
	 */
	parameter CarFrequencyCountSize 		= 694;
	parameter StartBurstSize 				= 192;
	parameter CarSelectBurstSize 			= 24;
	parameter GapSize 						= 24;
	parameter AssertBurstSize 				= 48;
	parameter DeAssertBurstSize 			= 24;

	input				CLK;			// 50MHz CLK signal
	input				RESET;			// RESET signal activated by switch
	input		[3:0]	COMMAND;		// COMMAND signal - sets direction
	input				SEND_PACKET;	// 10Hz counter
	output reg			PULSES;			// output pulses that control the car

	// Declare local registers
	reg			[3:0] CurrState, NextState;
	reg			[7:0] CurrBurstCounter, NextBurstCounter;

	/* this isn't necessary as CurrState has the same reset value */
// 	// initial CurrState and NextState value
// 	initial CurrState = 4'b0000;
// 	initial NextState = 4'b0000;

	// Generate the car frequency pulses from the 50MHz CLK
	reg					Pulse;
	reg					Pulse_Delay;
	reg			[9:0]	FrequencyCounter;
	always @( posedge CLK or posedge RESET) begin /* TODO: may need to revert */
		if( RESET ) begin
			Pulse 			<= `LOW;
			Pulse_Delay		<= `LOW;
			FrequencyCounter <= 0;
		end
		else begin
			if( FrequencyCounter == CarFrequencyCountSize ) begin
				Pulse 			<= ~Pulse;
				FrequencyCounter <= 0;
			end
			else begin
				FrequencyCounter <= FrequencyCounter + 1'b1;
			end
			Pulse_Delay 		<= Pulse;
		end
	end

	// Define local parameters for the states
	localparam	IDLE = 4'd0;
	localparam	Start = 4'd1;
	localparam	Gap1 = 4'd2;
	localparam	CarSelect = 4'd3;
	localparam	Gap2 = 4'd4;
	localparam	Right = 4'd5;
	localparam	Gap3 = 4'd6;
	localparam	Left = 4'd7;
	localparam	Gap4 = 4'd8;
	localparam	Backward = 4'd9;
	localparam	Gap5 = 4'd10;
	localparam	Forward = 4'd11;
	localparam	Gap6 = 4'd12;
	localparam	Finished = 4'd13;

	// State Machine
	// Sequential Logic
	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			CurrState <= 4'b0000;
			CurrBurstCounter <= 0;
		end
		else begin
			CurrState <= NextState;
			CurrBurstCounter <= NextBurstCounter;
		end
	end


	// Combinational Logic
	always @( * ) begin
		// setting the default
		NextState = CurrState;
		NextBurstCounter = 0;


		case( CurrState )

			// IDLE State
			IDLE: begin
				if( SEND_PACKET )
					NextState = Start;
				else
					NextState = CurrState;
			end

			// Start State
			Start: begin
				if( CurrBurstCounter == StartBurstSize - 1 ) begin
					NextState = Gap1;
					NextBurstCounter = 0;
				end
				else begin
					NextState = CurrState;
					if( Pulse_Delay & ~Pulse )
						NextBurstCounter 	= CurrBurstCounter + 1'b1;
					else
						NextBurstCounter 	= CurrBurstCounter;
				end
			end

			// Gap1 State
			Gap1: begin
				if( CurrBurstCounter == GapSize - 1 ) begin
					NextState = CarSelect;
					NextBurstCounter = 0;
				end
				else begin
					NextState = CurrState;
					if( Pulse_Delay & ~Pulse )
						NextBurstCounter 	= CurrBurstCounter + 1'b1;
					else
						NextBurstCounter 	= CurrBurstCounter;
				end
			end

			// Car Select State
			CarSelect: begin
				if( CurrBurstCounter == CarSelectBurstSize - 1 ) begin
					NextState = Gap2;
					NextBurstCounter = 0;
				end
				else begin
					NextState = CurrState;
					if( Pulse_Delay & ~Pulse )
						NextBurstCounter 	= CurrBurstCounter + 1'b1;
					else
						NextBurstCounter 	= CurrBurstCounter;
				end
			end

			// Gap2 State
			Gap2: begin
				if( CurrBurstCounter == GapSize - 1 ) begin
					NextState = Right;
					NextBurstCounter = 0;
				end
				else begin
					NextState = CurrState;
					if (Pulse_Delay & ~Pulse)
						NextBurstCounter 	= CurrBurstCounter + 1'b1;
					else
						NextBurstCounter 	= CurrBurstCounter;
				end
			end

			// Right State
			Right: begin
				if( ( COMMAND[0] & ( CurrBurstCounter >
									 AssertBurstSize - 1 ) ) |
					( ~COMMAND[0] & ( CurrBurstCounter >
									  DeAssertBurstSize - 1 ) ) ) begin
					NextState = Gap3;
					NextBurstCounter = 0;
				end
				else begin
					NextState = CurrState;
					if( Pulse_Delay & ~Pulse )
						NextBurstCounter 	= CurrBurstCounter + 1'b1;
					else
						NextBurstCounter 	= CurrBurstCounter;
				end
			end

			// Gap3 State
			Gap3: begin
				if( CurrBurstCounter == GapSize - 1 ) begin
					NextState = Left;
					NextBurstCounter = 0;
				end
				else begin
					NextState = CurrState;
					if( Pulse_Delay & ~Pulse )
						NextBurstCounter 	= CurrBurstCounter + 1'b1;
					else
						NextBurstCounter 	= CurrBurstCounter;
				end
			end

			// Left State
			Left: begin
				if( ( COMMAND[1] & ( CurrBurstCounter >
									 AssertBurstSize - 1 ) ) |
					( ~COMMAND[1] & ( CurrBurstCounter >
									  DeAssertBurstSize - 1 ) ) ) begin
					NextState = Gap4;
					NextBurstCounter = 0;
				end
				else begin
					NextState = CurrState;
					if( Pulse_Delay & ~Pulse )
						NextBurstCounter 	= CurrBurstCounter + 1'b1;
					else
						NextBurstCounter 	= CurrBurstCounter;
				end
			end

			// Gap4 State
			Gap4: begin
				if( CurrBurstCounter == GapSize - 1 ) begin
					NextState = Backward;
					NextBurstCounter = 0;
				end
				else begin
					NextState = CurrState;
					if( Pulse_Delay & ~Pulse )
						NextBurstCounter 	= CurrBurstCounter + 1'b1;
					else
						NextBurstCounter 	= CurrBurstCounter;
				end
			end

			// Backward State
			Backward: begin
				if( ( COMMAND[2] & ( CurrBurstCounter >
									 AssertBurstSize - 1 ) ) |
					( ~COMMAND[2] & ( CurrBurstCounter >
									  DeAssertBurstSize - 1 ) ) ) begin
					NextState = Gap5;
					NextBurstCounter = 0;
				end
				else begin
					NextState = CurrState;
					if( Pulse_Delay & ~Pulse )
						NextBurstCounter 	= CurrBurstCounter + 1'b1;
					else
						NextBurstCounter 	= CurrBurstCounter;
				end
			end

			// Gap5 State
			Gap5: begin
				if( CurrBurstCounter == GapSize - 1 ) begin
					NextState = Forward;
					NextBurstCounter = 0;
				end
				else begin
					NextState = CurrState;
					if( Pulse_Delay & ~Pulse )
						NextBurstCounter 	= CurrBurstCounter + 1'b1;
					else
						NextBurstCounter 	= CurrBurstCounter;
				end
			end

			// Forward State
			Forward: begin
				if( ( COMMAND[3] & ( CurrBurstCounter >
									 AssertBurstSize - 1 ) ) |
					( ~COMMAND[3] & ( CurrBurstCounter >
									  DeAssertBurstSize - 1 ) ) ) begin
					NextState = Gap6;
					NextBurstCounter = 0;
				end
				else begin
					NextState = CurrState;
					if( Pulse_Delay & ~Pulse )
						NextBurstCounter 	= CurrBurstCounter + 1'b1;
					else
						NextBurstCounter 	= CurrBurstCounter;
				end
			end

			// Gap6 State
			Gap6: begin
				if(CurrBurstCounter == GapSize - 1) begin
					NextState = Finished;
					NextBurstCounter = 0;
					end
					else begin
						NextState = CurrState;
					if (Pulse_Delay & ~Pulse)
						NextBurstCounter	= CurrBurstCounter + 1'b1;
					else
						NextBurstCounter	= CurrBurstCounter;
				end
			end

			// Finished State
			Finished: begin
				NextState			= IDLE;
			end

			// Default State
			default: begin
				NextState			= IDLE;
				NextBurstCounter	= 0;
			end

		endcase
	end

	// Output Logic for the PULSES
	always @( * ) begin
		case( NextState )
			IDLE:		PULSES = 0;
			Start:		PULSES = Pulse;
			Gap1:		PULSES = 0;
			CarSelect:	PULSES = Pulse;
			Gap2:		PULSES = 0;
			Right:		PULSES = Pulse;
			Gap3:		PULSES = 0;
			Left:		PULSES = Pulse;
			Gap4:		PULSES = 0;
			Backward:	PULSES = Pulse;
			Gap5:		PULSES = 0;
			Forward:	PULSES = Pulse;
			Gap6:		PULSES = 0;
			Finished:	PULSES = 0;
			default:	PULSES = 0;
		endcase
	end

endmodule
