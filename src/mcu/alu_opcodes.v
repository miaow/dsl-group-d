`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
// 
// Create Date:		22:35:52 01/31/2014
// Design Name:		MCU
// Module Name:		alu_opcodes
// Project Name:	dsl_group_d
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		ALU opcode macros for improved readability
//
// Dependencies:	none
//
// Revision:
// v0.02 - 201401312242Z - Implementation complete
// v0.01 - 201401312235Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////

`define ALU_ADD			4'h0	/* return A + B */
`define ALU_SUB			4'h1	/* return A - B */
`define ALU_MUL			4'h2	/* return A * B */
`define ALU_SLL			4'h3	/* return A << 1 */
`define ALU_SRL			4'h4	/* return A >> 1 */
`define ALU_INA			4'h5	/* return A + 1 */
`define ALU_INB			4'h6	/* return B + 1 */
`define ALU_DEA			4'h7	/* return A - 1 */
`define ALU_DEB			4'h8	/* return B - 1 */
`define ALU_EQU			4'h9	/* return A == B */
`define ALU_GRT			4'hA	/* return A > B */
`define ALU_LES			4'hB	/* return A < B */
`define ALU_AND			4'hC	/* return A & B */
`define ALU_XOR			4'hD	/* return A ^ B */
`define ALU_OR			4'hE	/* return A | B */