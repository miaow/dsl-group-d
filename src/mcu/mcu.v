`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
// 
// Create Date:		22:53:52 01/31/2014
// Design Name:		MCU
// Module Name:		alu_opcodes
// Project Name:	dsl_group_d
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		the MCU itself
//
// Dependencies:	none
//
// Revision:
// v0.02 - 20140131xxxxZ - Implementation complete
// v0.01 - 201401312253Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////
`include "../globals.v"
`define REG_SELECT_A 1'b0
`define REG_SELECT_B 1'b1

module mcu
(
	input			CLK,
	input			RESET,
	/* bus signals */
	inout	[7:0]	BUS_DATA,
	output	[7:0]	BUS_ADDR,
	output			BUS_DATA_OE,
	input	[1:0]	BUS_IRQ,
	output	[1:0]	BUS_ACK,
	/* ROM signals */
	output	[7:0]	ROM_ADDR,
	input	[7:0]	ROM_DATA

);



	wire	[7:0]	BUS_DATA_IN;
	reg		[7:0]	curr__BUS_DATA_OUT, next__BUS_DATA_OUT;
	reg				curr__BUS_DATA_OE, next__BUS_DATA_OE;

	/* tristate bus signals */
	assign BUS_DATA_IN		= BUS_DATA;
	assign BUS_DATA			= curr__BUS_DATA_OE ? curr__BUS_DATA_OUT : 8'hzz;
	assign BUS_DATA_OE		= curr__BUS_DATA_OE;

	/* state of the address bus */
	reg		[7:0]	curr__BUS_ADDR, next__BUS_ADDR;
	assign BUS_ADDR			= curr__BUS_ADDR;

	/* The MCU has 2 internal registers to hold operands and one to hold the
	 * context when doing f-n calls
	 */
	reg		[7:0]	curr__reg_a, next__reg_a;
	reg		[7:0]	curr__reg_b, next__reg_b;
	reg				curr__reg_select, next__reg_select;
	reg		[7:0]	curr__context, next__context;

	/* dedicated IRQ ACK lines, one for each IRQ */
	reg		[1:0]	curr__BUS_ACK, next__BUS_ACK;
	assign BUS_ACK = curr__BUS_ACK;

	/* instantiate the program memory control lines
	 * The Program Counter (PC) points to the current op. It also has an offset
	 * that is used to reference data that is part of the current operation.
	 */
	reg		[7:0]	curr__pc, next__pc;
	reg		[1:0]	curr__pc_offset, next__pc_offset;
	assign ROM_ADDR = curr__pc + curr__pc_offset;
	
	/* instantiate the ALU (see alu.v) */
	wire	[7:0]	alu_out;
	alu alu0
	(
		.CLK(CLK),
		.RESET(RESET),
		/* I/O */
		.ALU_IN_A(curr__reg_a),
		.ALU_IN_B(curr__reg_b),
		.ALU_OPCODE(ROM_DATA[7:4]),
		.ALU_OUT(alu_out)
	);

	/* The MCU is a state machine with a sequential pipeline
	 * The supported list of operations is:
	 * 0  - load word from memory into A
	 * 1  - load word from memory into B
	 * 2  - store word into memory from A
	 * 3  - store word into memory from B
	 * 4  - issue an ALU op, store result in A
	 * 5  - issue an ALU op, store result in B
	 * 6  - conditional (==, < or >) branch to address
	 * 7  - unconditional jump
	 * 8  - go to idle state (low power, no operations, wait for interrupt)
	 * 9  - function call
	 * 10 - return from function call
	 * 11 - dereference A
	 * 12 - dereference B
	 */

	parameter	[7:0]	/* program thread selection */
	state_idle						= 8'hF0, /* wait until IRQ wakes the MCU */
	state_get_thread_start_addr_0	= 8'hF1, /* wait */
	state_get_thread_start_addr_1	= 8'hF2, /* apply the new addr to PC */
	state_get_thread_start_addr_2	= 8'hF3, /* wait, go to state_choose_op */
	/* operation selection - depending on the value of ROM_DATA, go to one of
	 * the instruction start states
	 */
	state_reset						= 8'hFF,
	state_choose_op					= 8'h00,

	/* data flow */
	state_lw_into_a					= 8'h10, /* find what addr to read, save..*/
	state_lw_into_b					= 8'h11, /* ..reg select */
	state_lw_0						= 8'h12, /* set BUS_ADDR */
	state_lw_1						= 8'h13, /* wait; PC+=2, PC_OFFSET=0 */
	state_lw_2						= 8'h14, /* (A|B) <= BUS_DATA */
	state_sw_from_a					= 8'h20, /* read PC+1 to get ADDR */
	state_sw_from_b					= 8'h21, /* as above */
	state_sw_0						= 8'h22, /* wait; PC+=2, PC_OFFSET=0 */
	state_sw_1						= 8'h23,

	/* data manipulation */
	state_alu_op_into_a				= 8'h30, /* result from ALU is rdy, save */
	state_alu_op_into_b				= 8'h31, /* as above */
	state_alu_op_0					= 8'h32, /* wait for new op addr to 
											  * settle, end op */

	/* control flow */
	/* branches */
	state_branch					= 8'h40,
	state_branch_0					= 8'h41, /*goto the load memory ADDR*/
	state_branch_1					= 8'h42, /*go to state_choose_op*/
	state_branch_2					= 8'h43,
	/* jmp - unconditional jump */
	state_jmp						= 8'h50, /* load address from memory */
	state_jmp_0						= 8'h51, /* jmp then state_choose_op */
	state_jmp_1						= 8'h52,
	/* function call */
	state_call_0					= 8'h55,
	state_call_1					= 8'h56,
	/* function return */
	state_ret						= 8'h58,
	/* go to idle state */
	state_sleep						= 8'h68,

	/* dereference a or b (i.e put contents of address that a/b points to in
	 * a or b)
	 */
	state_deref_a					= 8'h60,
	state_deref_b					= 8'h61,
	state_deref_0					= 8'h62,
	state_deref_1					= 8'h63,
	state_deref_2					= 8'h64;

	/* sequential part of the state machine */
	reg		[7:0]	curr__state, next__state;

	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			curr__state				<= state_reset;
			curr__pc				<= 8'h00;
			curr__pc_offset			<= 2'b00;
			curr__BUS_ADDR			<= 8'h00;
			curr__BUS_DATA_OUT		<= 8'h00;
			curr__BUS_DATA_OE		<= `FALSE;
			curr__reg_a				<= 8'h00;
			curr__reg_b				<= 8'h00;
			curr__reg_select		<= `REG_SELECT_A;
			curr__context			<= 8'h00;
			curr__BUS_ACK			<= 2'b00;
		end
		else begin
			curr__state				<= next__state;
			curr__pc				<= next__pc;
			curr__pc_offset			<= next__pc_offset;
			curr__BUS_ADDR			<= next__BUS_ADDR;
			curr__BUS_DATA_OUT		<= next__BUS_DATA_OUT;
			curr__BUS_DATA_OE		<= next__BUS_DATA_OE;
			curr__reg_a				<= next__reg_a;
			curr__reg_b				<= next__reg_b;
			curr__reg_select		<= next__reg_select;
			curr__context			<= next__context;
			curr__BUS_ACK			<= next__BUS_ACK;
		end
	end

	/* combinatorial section - may the Cosmos help you */
	always @( * ) begin
		/* assign some defaults to avoid bloat in the rest of the states */
		next__state					= curr__state;
		next__pc					= curr__pc;
		next__pc_offset				= 2'h0;
		next__BUS_ADDR				= 8'hFF;
		next__BUS_DATA_OUT			= curr__BUS_DATA_OUT;
		next__BUS_DATA_OE			= `FALSE;
		next__reg_a					= curr__reg_a;
		next__reg_b					= curr__reg_b;
		next__reg_select			= curr__reg_select;
		next__context				= curr__context;
		next__BUS_ACK				= 2'b00;
		
		/* state logic */
		case( curr__state )

			/* thread selection states */
			state_idle: begin
				if( BUS_IRQ[0] ) begin /* IRQ 0 - base addr 0xFF */
					next__state		= state_get_thread_start_addr_0;
					next__pc		= 8'hFF;
					next__BUS_ACK	= 2'b01;
				end
				else if( BUS_IRQ[1] ) begin /* IRQ 1 - base addr 0xFE */
					next__state		= state_get_thread_start_addr_0;
					next__pc		= 8'hFE;
					next__BUS_ACK	= 2'b10;
				end
				else begin
					next__state		= state_idle;
					next__pc		= 8'hFF; /* nothing has happened */
					next__BUS_ACK	= 2'b00;
				end
			end

			/* wait state */
			state_get_thread_start_addr_0: begin
				next__state			= state_get_thread_start_addr_1;
			end

			/* PC <= ROM_DATA */
			state_get_thread_start_addr_1: begin
				next__state			= state_get_thread_start_addr_2;
				next__pc			= ROM_DATA;
			end

			/* wait for the new PC to settle */
			state_get_thread_start_addr_2: begin
				next__state			= state_choose_op;
			end

			/*------------------------------------------------------------------
			 * choose op - another case statement to determine the operation
			 */
			state_reset: begin
				next__state			= state_choose_op;
			end
			state_choose_op: begin
				case( ROM_DATA[3:0] )

				4'h0:	next__state	= state_lw_into_a;
				4'h1:	next__state	= state_lw_into_b;
				4'h2:	next__state	= state_sw_from_a;
				4'h3:	next__state	= state_sw_from_b;
				4'h4:	next__state	= state_alu_op_into_a;
				4'h5:	next__state	= state_alu_op_into_b;
				
				
				4'h6:	next__state	= state_branch;
				4'h7:	next__state	= state_jmp;
				4'h8:	next__state	= /* state_idle */ state_sleep;
				4'h9:	next__state	= state_call_0;
				4'hA:	next__state	= state_ret;
				4'hB:	next__state	= state_deref_a;
				4'hC:	next__state	= state_deref_b;
				default:
						next__state	= curr__state;

				endcase
				next__pc_offset		= 2'b01;
			end

			/*------------------------------------------------------------------
			 * load word: start of the memory read sequence.
			 * wait state - reg_select is 0 for load into A, 1 for load into B
			 */
			state_lw_into_a: begin
				next__state			= state_lw_0;
				next__reg_select	= `REG_SELECT_A;
			end

			state_lw_into_b: begin
				next__state			= state_lw_0;
				next__reg_select	= `REG_SELECT_B;
			end

			/* ROM_DATA will be valid, so set the bus address value */
			state_lw_0: begin
				next__state			= state_lw_1;
				next__BUS_ADDR		= ROM_DATA;
			end

			/* wait for BUS_DATA to settle. Inc PC here. This must be done 2
			 * cycles ahead of time so thta it gives the proper data when
			 * needed.
			 */
			state_lw_1: begin
				next__state			= state_lw_2;
				next__pc			= curr__pc + 2;
			end

			/* data is now available. Write to the proper register */
			state_lw_2: begin
				next__state			= state_choose_op;

				if( curr__reg_select == `REG_SELECT_A )
					next__reg_a		= BUS_DATA_IN;
				else
					next__reg_b		= BUS_DATA_IN;
			end

			/*------------------------------------------------------------------
			 * store word: start of the memory write sequence
			 * wait state - get target memory address; increment PC 2 cycles
			 * ahead of time
			 */
			state_sw_from_a: begin
				next__state			= state_sw_0;
				next__reg_select	= `REG_SELECT_A;
				next__pc			= curr__pc + 2;
			end

			state_sw_from_b: begin
				next__state			= state_sw_0;
				next__reg_select	= `REG_SELECT_B;
				next__pc			= curr__pc + 2;
			end

			/* address is now valid, so set BUS_ADDR and the value to the mem
			 * location
			 */
			state_sw_0: begin
				next__state			= state_choose_op;
				next__BUS_ADDR		= ROM_DATA;
				next__BUS_DATA_OE	= `TRUE;

				if( curr__reg_select == `REG_SELECT_A )
					next__BUS_DATA_OUT	= curr__reg_a;
				else
					next__BUS_DATA_OUT	= curr__reg_b;
			end


			/* alu_op: start of the ALU operation sequence
			 * reg_a and reg_b have to already be set to the desired values
			 * before issuing the op. The result is ready in this cycle
			 */
			state_alu_op_into_a: begin
				next__state			= state_alu_op_0;
				next__reg_a			= alu_out;
				next__pc			= curr__pc + 1;
			end

			state_alu_op_into_b: begin
				next__state			= state_alu_op_0;
				next__reg_b			= alu_out;
				next__pc			= curr__pc + 1;
			end

			state_alu_op_0: begin

				next__state			= state_choose_op;
			end


			/* branch: check if the ALU returns true or not (resolve the branch)
			 * if true, update branch with target address.
			 * if false, pc <= pc + 2
			 */
			state_branch: begin
				if(alu_out == 8'h01) begin
					next__state		= state_branch_0;
				end
				else begin
					next__state		= state_branch_1;
				end
			end

			/* rom data is ready to be read */
			state_branch_0: begin
				next__state			= state_branch_2;
				next__pc			= ROM_DATA;
			end

			state_branch_1: begin
				next__pc			= curr__pc + 2;
				next__state			= state_branch_2;
			end

			state_branch_2: begin
				next__state			= state_choose_op;
			end


			/* jump - unconditionally change the program counter to the value
			 * in ROM
			 */
			state_jmp: begin
				next__state			= state_jmp_0;
			end

			/* data is ready to load into program counter */
			state_jmp_0:begin
				next__pc			= ROM_DATA;
				next__state			= state_jmp_1;
			end

			state_jmp_1 : begin
				next__state			= state_choose_op;
			end


			/* go to idle state */
			state_sleep: begin
				next__pc			= 8'hFF;
				next__state			= state_idle;
			end


			/* function call - store the context (i.e. only the PC)... */
			state_call_0: begin
				next__context		= curr__pc + 2;
				next__state			= state_call_1;
			end

			/* ... and jump to the target address */
			state_call_1: begin
				next__pc			= ROM_DATA;
				next__state			= state_choose_op;
			end


			/* return from function call - restore the context and go to
			 * choose op state
			 */
			state_ret: begin
				next__pc			= curr__context;
				next__state 		= state_choose_op;
			end


			/* dereference operation: get value from address in RAM as given in
			 * register
			 */
			state_deref_a: begin
				next__state			= state_deref_0;
				next__reg_select	= `REG_SELECT_A;
				next__BUS_ADDR		= curr__reg_a;
			end

			state_deref_b: begin
				next__state			= state_deref_0;
				next__reg_select	= `REG_SELECT_B;
				next__BUS_ADDR		= curr__reg_b;
			end

			/* set the memory address in register to address bus */
			state_deref_0: begin
				next__state			= state_deref_1;
				next__pc			= curr__pc + 1;
			end
			
			/* write the data memory into register */
			state_deref_1: begin
				next__state			= state_choose_op;
				if( curr__reg_select == `REG_SELECT_A )
					next__reg_a		= BUS_DATA_IN;
				else
					next__reg_b		= BUS_DATA_IN;
			end

		endcase
	end

endmodule
