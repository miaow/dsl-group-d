`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
// 
// Create Date:		22:35:52 01/31/2014
// Design Name:		MCU
// Module Name:		alu_opcodes
// Project Name:	dsl_group_d
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		ALU for the MCU
//
// Dependencies:	none
//
// Revision:
// v0.02 - 201401312252Z - Implementation complete
// v0.01 - 201401312235Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////
`include "alu_opcodes.v"

module alu
(
	input				CLK,
	input				RESET,
	/* alu inputs */
	input		[7:0]	ALU_IN_A,
	input		[7:0]	ALU_IN_B,
	input		[3:0]	ALU_OPCODE,
	output reg	[7:0]	ALU_OUT
);

	always @( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			ALU_OUT		<= 8'h00;
		end
		else begin
			case( ALU_OPCODE )
				`ALU_ADD:	ALU_OUT <= ALU_IN_A + ALU_IN_B;
				`ALU_SUB:	ALU_OUT <= ALU_IN_A - ALU_IN_B;
				`ALU_MUL:	ALU_OUT	<= ALU_IN_A * ALU_IN_B;
				`ALU_SLL:	ALU_OUT <= ALU_IN_A << 1;
				`ALU_SRL:	ALU_OUT <= ALU_IN_A >> 1;
				`ALU_INA:	ALU_OUT <= ALU_IN_A + 1;
				`ALU_INB:	ALU_OUT <= ALU_IN_B + 1;
				`ALU_DEA:	ALU_OUT <= ALU_IN_A - 1;
				`ALU_DEB:	ALU_OUT <= ALU_IN_B - 1;
				`ALU_EQU:	ALU_OUT <= ( ALU_IN_A == ALU_IN_B ) ? 8'h01 : 8'h00;
				`ALU_GRT:	ALU_OUT <= ( ALU_IN_A > ALU_IN_B ) ? 8'h01 : 8'h00;
				`ALU_LES:	ALU_OUT	<= ( ALU_IN_A < ALU_IN_B ) ? 8'h01 : 8'h00;
				`ALU_AND:	ALU_OUT <= ALU_IN_A & ALU_IN_B;
				`ALU_XOR:	ALU_OUT <= ALU_IN_A ^ ALU_IN_B;
				`ALU_OR:	ALU_OUT <= ALU_IN_A | ALU_IN_B;
				default:	ALU_OUT <= ALU_IN_A;
			endcase
		end
	end

endmodule