`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
//
// Create Date:		15:10:33 01/30/2014
// Design Name:		MCU
// Module Name:		ram
// Project Name:	dsl_group_d
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		The RAM for the MCU.
//
// Dependencies:	none
//
// Revision:
// v0.02 - 201401301527Z - Implementation complete
// v0.01 - 201401301510Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////
`include "globals.v"

module ram
(
	input			CLK,
	/* bus signals */
	inout	[7:0]	BUS_DATA,
	input	[7:0]	BUS_ADDR,
	input			BUS_DATA_OE
);

	parameter ram_base_addr		= 0;
	parameter ram_addr_width	= 7; /* 7-bit address for 128 lines of 8 bits */
	
	/* BUS_DATA tristate setup */
	wire	[7:0]	BUS_DATA_IN;
	reg		[7:0]	BUS_DATA_OUT;
	reg				BUS_RAM_WE;
	assign BUS_DATA				= BUS_RAM_WE ? BUS_DATA_OUT : 8'hzz;
	assign BUS_DATA_IN			= BUS_DATA;
	
	/* RAM array */
	reg		[7:0] mem [2**ram_addr_width-1:0];
	/* initialise it with the .hex file */
	initial $readmemh( "ram.hex", mem );
	
	/* RAM has a single port only */
	always @( posedge CLK ) begin
		/* highest address is 0x7F => only need to check if BUS_ADDR[7] is
		 * set or not */
		if( ~BUS_ADDR[7] ) begin
			if( BUS_DATA_OE ) begin
				mem[BUS_ADDR[6:0]] <= BUS_DATA_IN;
				BUS_RAM_WE		<= `FALSE;
			end
			else begin
				BUS_RAM_WE		<= `TRUE;
			end
		end
		else begin
			BUS_RAM_WE			<= `FALSE;
		end

		BUS_DATA_OUT			<= mem[BUS_ADDR[6:0]];

	end

endmodule