# MCU source code

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# DATA SECTION
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
.data

#-------------------------------------------------------------------------------
# Common Variables
#-------------------------------------------------------------------------------
zero:
	0
one:
	1
two:
	2

#-------------------------------------------------------------------------------
# IR Variables
#-------------
ir_col0_cmd:
	0
ir_col1_cmd:
	4
ir_col2_cmd:
	8
ir_row0_cmd:
	0
ir_row1_cmd:
	1
ir_row2_cmd:
	2
ir_cmd:
	5
ir_select_mask:
	240

#-------------------------------------------------------------------------------
# PS/2 Variables
#---------------
ps2_x_coord:
	80
ps2_y_coord:
	60
ps2_z_coord:
	0
ps2_status_mask:
	15
ps2_x_init:
	80
ps2_y_init:
	60

#-------------------------------------------------------------------------------
# VGA Variables
#--------------
# #A8
config_f_colour:
	168
# #00
config_b_colour:
	0
vga_x_addr:
	0
vga_y_addr:
	0
vga_col_0:
	54
vga_col_1:
	108
vga_row_0:
	40
vga_row_1:
	80
config_f_colour_init:
	168
config_b_colour_init:
	0

porval:
	0


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# TEXT SECTION
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
.text

startup:
	lw $a @porval
	lw $b @zero
# if a POR, then only add the new cursor
	beq @por
#else (this is a button-event reset)
# load the old (x,y) coordinates and remove the cursor from there
	sw $b @vga_0
	lw $a @ps2_x_coord
	sw $a @vga_2
	lw $a @ps2_y_coord
	sw $a @vga_1
	lw $b @one
	sw $b @vga_0
	lw $a @vga_1
	sw $a @vga_1
	lw $a @ps2_x_init
	sw $a @ps2_x_coord
	lw $b @zero
	sw $b @vga_0
	sw $a @vga_2
	lw $a @ps2_y_init
	sw $a @ps2_y_coord
	sw $a @vga_1
por:
	lw $b @one
	sw $b @porval
# set the mouse scrollwheel support
	lw $a @buttons
	lw $b @one
	and $a
	lw $b @two
	sw $b @mouse_0
	sw $a @mouse_1
# rescind the reset signal to the mouse
	sw $a @mouse_0
# flip the frame buffer bit
	lw $a @one
	sw $a @vga_0
	lw $a @vga_1
	sw $a @vga_1
#	sw $b @leds
# reset fg and bg colours in RAM
	lw $a @config_f_colour_init
	sw $a @config_f_colour
	lw $a @config_b_colour_init
	sw $a @config_b_colour
	idle $a
# 66 bytes


mouse_irq:
#-------------------------------------------------------------------------------
# STEP 1: --- load x byte and send to 7-segment display ---
# STEP 2: --- load y byte and send to 7-segment display ---
#-------------------------------------------------------------------------------
	lw $a @mouse_1
	lw $b @mouse_2
	sw $a @ps2_x_coord
	sw $b @ps2_y_coord
	sw $a @seven_0
	sw $b @seven_1
#-------------------------------------------------------------------------------
# STEP 3: z + status bits
#-------------------------------------------------------------------------------
# set the ps2 driver to output z @mouse_0
	lw $a @one
	sw $a @mouse_0
	lw $a @mouse_0
	sw $a @ps2_z_coord
# shift the result left by 4
	sll $a
	sll $a
	sll $a
	sll $a
# load status byte, stripped in hardware so no need to adjust; or with z coord
# and send to LEDs
	lw $b @zero
	sw $b @mouse_0
	lw $b @mouse_0
	or $a
	sw $a @leds
# 33 bytes

colouring_time:
	lw $a @mouse_0
	lw $b @one
	and $a
	beq @colour_fg
colour_fg_end:
	lw $a @mouse_0
	lw $b @two
	and $a
	beq @colour_bg
	jmp @vga_start
colour_fg:
	lw $a @two
	sw $a @vga_0
	lw $a @config_f_colour
	lw $b @ps2_z_coord
	add $a
	sw $a @vga_1
	sw $a @config_f_colour
	jmp @colour_fg_end
colour_bg:
	lw $a @two
	sw $a @vga_0
	lw $a @config_b_colour
	lw $b @ps2_z_coord
	add $a
	sw $a @vga_2
	sw $a @config_b_colour

vga_start:
# flip FB bit
	lw $b @one
	sw $b @vga_0
	lw $a @vga_1
	sw $a @vga_1
# change X and Y addresses
	lw $a @zero
	sw $a @vga_0
	lw $a @ps2_x_coord
	sw $a @vga_2
	lw $a @ps2_y_coord
	sw $a @vga_1
# flip FB bit
	sw $b @vga_0
	lw $a @vga_1
	sw $a @vga_1
# 26 bytes

# determine the IR command that needs to be sent to the IR module
ir_col_calc:
	lw $a @ps2_x_coord
	lw $b @vga_col_0
	blt @ir_col_0
	lw $b @vga_col_1
	blt @ir_col_1
ir_col_2:
	lw $a @ir_col2_cmd
	sw $a @ir_cmd
	jmp @ir_row_calc
ir_col_1:
	lw $a @ir_col1_cmd
	sw $a @ir_cmd
	jmp @ir_row_calc
ir_col_0:
	lw $a @ir_col0_cmd
	sw $a @ir_cmd
#	jmp @ir_row_calc

ir_row_calc:
	lw $a @ps2_y_coord
	lw $b @vga_row_0
	blt @ir_row_0
	lw $b @vga_row_1
	blt @ir_row_1
ir_row_2:
	lw $a @ir_cmd
	lw $b @ir_row2_cmd
	or $a
	sw $a @ir_cmd
#	jmp @ir_calc_end
	idle $a
ir_row_1:
	lw $a @ir_cmd
	lw $b @ir_row1_cmd
	or $a
	sw $a @ir_cmd
#	jmp @ir_calc_end
	idle $a
ir_row_0:
	lw $a @ir_cmd
	lw $b @ir_row0_cmd
	or $a
	sw $a @ir_cmd
#	jmp @ir_calc_end
ir_calc_end:
	idle $a
# 62 bytes

timer_irq:
	lw $a @buttons
	lw $b @ir_select_mask
	and $a
	lw $b @ir_cmd
	or $a
	sw $a @ir
#	sw $a @leds
	idle $a
# 11 bytes

irq_0:
	@mouse_irq
irq_1:
	@timer_irq
