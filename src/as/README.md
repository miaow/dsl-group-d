README
======

This is the folder containing the assembler for the architecture of the MCU. The
code compiles for GNU/Linux, and has toolchain support for cross-compiling a
Windows binary (PE format). It has only been thoroughly tested on GNU/Linux,
hence using the Windows build is discouraged.

## Compiling

To compile on GNU/Linux, you'll need `cmake` and `gcc` with support for C++11.

    mkdir build
    cd build
    cmake ..
    make

To cross-compile for Windows, you'll need the `mingw32` compiler installed in a
system directory.

    mkdir build-win
    cd build-win
    cmake -DCMAKE_TOOLCHAIN_FILE=../toolchain-mingw32.cmake ..
    make

The toolchain file expects the `mingw32` binaries to be named a specific way. If
the naming is different on your distro, then please modify the toolchain file to
account for that.

## Folder Structure

  `src`: all implementation files go here;
  `build`: the compiled binary of the assembler.

## Assembler usage

To assemble a file, place it in the `src/as` directory, and from there:

    ./build/as ./<filename>.s && mv ./*.hex ../

This will compile the `*.hex` files used by Xilinx and place them where they
will be found by ISE to use when synthesizing.

## Source structure

The source code for the assembler is split into a main file and an assembler
class `ASM`. The majority of the work is done in the main file, whereas the
`ASM` class only reads the definition files, and also assembles a single
instruction from a line of text and returns the byte(s) for it. The labels,
operation mnemonics, and registers are defined in separate text files which are
parsed by the assembler. This way, there is no need to recompile the assembler
each time a new instruction is added (as long as the new instruction conforms to
one of the two existing instruction formats), which happened a few times.

In essence, the assembler makes two passes through the source file. In the first
pass, it records the label positions and the size of data/code following those
labels. In the second pass, it resolves the labels' addresses and assembles each
instruction from the .text section. The assembly can only be done once all the
labels are resolved, hence the need for a two-pass system.
