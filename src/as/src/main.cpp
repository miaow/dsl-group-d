 /* DSL MCU ASSEMBLER
 * Author: Mihail Atanassov
 * Matric: s1018353
 *
 * Notes: ---
 */

#include<iostream>
#include<bitset>
#include<cstdlib>
#include<inttypes.h>
#include<cstdio>
#include<cstring>
#include<boost/tokenizer.hpp>
#include<vector>

#include "include/asm.h"
//#include<>

/* bitstream containers */
struct ram_t
{
	uint8_t val[RAM_SIZE];
	bitset<RAM_SIZE> used;
	uint8_t lfa = 0x00;
} ram;
struct rom_t
{
	uint8_t val[ROM_SIZE];
	bitset<ROM_SIZE> used;
	uint8_t lfa = 0x00;
} rom;

/* fwd declaration */
void fill_ram(uint8_t& elems_size, uint8_t& num_elems, char* line, string& last_label, const string& str_line, vector< uint64_t >& elems);

using namespace std;
using namespace boost;

ASM assembler;
int main(int argc, char** argv)
{
	/* check if argument is given */
	if( argc < 2 )
	{
		cout << "Too few args" << endl;
		return EXIT_FAILURE;
	}

	int retval = 0;

	if( !assembler.read_instr_defs("./src/ops.def") )
	{
		cout << "Failed to read instruction definitions" << endl;
		retval = EXIT_FAILURE;
	}
	if( !assembler.read_reg_defs("./src/regs.def") )
	{
		cout << "Failed to read register definitions" << endl;
		retval = EXIT_FAILURE;
	}
	if( !assembler.read_label_defs("./src/labels.def") )
	{
		cout << "Failed to read label definitions" << endl;
		retval = EXIT_FAILURE;
	}
	if(retval)
		return retval;

	/* continue with the actual assembly */


	for( int i = 0; i < RAM_SIZE; ++i )
	{
		ram.val[i] = 0;
		ram.used[i] = 0;
	}

	for( int i = 0; i < ROM_SIZE; ++i )
	{
		rom.val[i] = 0;
		rom.used[i] = 0;
	}

	/* read the file, C-style (5x faster) */

	FILE* fh = NULL;
	if( ( fh = fopen( argv[1], "r" ) ) == NULL )
	{
		cout << "Could not open file" << endl;
		return EXIT_FAILURE;
	}

	char line[82]; /* 80 character limit per line + \n + \0 */
	uint8_t mode = 0;
#define MODE_DATA 1
#define MODE_TEXT 2
	/* we've opened the file */
	vector< string > curr_stream;
	uint32_t curr_stream_length = 0;
	map<string, vector< string > > streams;
	map<string, uint32_t> stream_lengths;
	string last_label;
	/* Procedure:
	 * Pass1: find out the size of the instruction streams and resolve label
	 * 		addresses.
	 * Pass2: assemble the instructions
	 */

	/* PASS1 */
	/* stuff for filling up RAM */
	uint8_t num_elems = 0;
	uint8_t elems_size = 0; /* size in bytes */
	vector< uint64_t > elems;


	uint32_t curr_line = 0; /* line counter for better error output */
	while( fgets( line, sizeof line, fh ) != NULL )
	{
		++curr_line;
		if( line[0] == '\n' || line[0] == '#' )
			continue; /* skip empty and comment lines */
// 		cout << "non-empty line" << endl;
		/* tokenise each line and convert to a bit pattern */
		/* check if line contains a label */
		if( line[0] == ' ' || line[0] == '\t' )
		{
			if( mode == 0 )
				continue;
			if( mode == MODE_TEXT )
			{
				/* line should be an instruction. add to stream and find its
				 * length
				 */
				uint8_t len;
				string str_line(line);
				/* tokenise the string to grab the first word. That'll be the
				 * op itself
				 */
				char_separator< char > sep(" \t"); // separate on white space
				wstokenizer_t tok(str_line, sep);
				string op = *(tok.begin());
				if( op.c_str()[0] == '@' )
				{
					/* this is an address
					 * for handling irq_0 and irq_1
					 */
					curr_stream.emplace_back(str_line);
					curr_stream_length += 1; /* single byte for the address */
					continue;
				}
				if( assembler.find_op(op, len) )
				{
					cout << "Instruction " << op
						<< " len: " << (int) len << endl;
					curr_stream.emplace_back(str_line);
					curr_stream_length += len;
				}
				else
				{
					cout << "No such instruction: \"" <<
						str_line.substr(1, str_line.length()-2)
						<< "\" on line "
						<< curr_line << endl;
					exit(EXIT_FAILURE);
				}
// 				instr_t instruction;
// 				instruction = assembler.assemble(str_line);
// 				cout << instruction.length << " " << (int)instruction.byte1 <<
// 						" " << (int)instruction.byte0 << endl;
				/* TODO: create instruction streams and map them to labels.
				 * write resolved labelled streams to container. after file is
				 * completely read, assign addresses to unresolved labels and
				 * write out.
				 */
			}
			else
			{
				/* line is data and should go to RAM as-is */
				++num_elems;
				uint64_t elem;
				elem = strtoul(line, NULL, 10);
				if( (uint8_t) elem == elem )
				{
					cout << "8bit elem" << endl;
					if( elems_size < 1 )
						elems_size = 1;
				}
				else if( (uint16_t) elem == elem )
				{
					cout << "16bit elem" << endl;
					if( elems_size < 2 )
						elems_size = 2;
				}
				else if( (uint32_t) elem == elem )
				{
					cout << "32bit elem" << endl;
					if( elems_size < 4 )
						elems_size = 4;
				}
				else
				{
					cout << "64bit elem" << endl;
					elems_size = 8;
				}
				elems.emplace_back( elem );
			}
		}
		else
		{
			/* change mode if needed */
			if( strcmp(line, ".data\n") == 0 )
			{
				mode = MODE_DATA;
				cout << "mode data" << endl;
				continue;
			}
			if( strcmp(line, ".text\n") == 0 )
			{
				mode = MODE_TEXT;
				/* assign the last RAM array and its label before moving on */
// 				cout << "ADDING RAM Label: " << last_label << endl;
// 				assembler.add_label(last_label, ram.lfa);
				fill_ram(elems_size, num_elems,
						 line, last_label,
						 string(""), elems);

				/* print the hex file */
				char ram_str[RAM_SIZE*3 + 1];
				for( int i = 0; i < RAM_SIZE; ++i )
				{
// 					cout << "RAM[" << i << "]: " << (int)ram.val[i] << endl;
					sprintf( ram_str + 3*i, "%02x\n", ram.val[i] );
				}
				FILE* ram_fh = fopen( "ram.hex", "w" );
				fprintf( ram_fh, "%s", ram_str );
				fclose(ram_fh);
				cout << "mode text" << endl;
				continue;
			}

			if( mode == 0 )
				continue;
			/* assign label */
			string str_line(line);
			if( str_line.substr(str_line.length() - 2) != ":\n" )
			{
				cout << "ERROR: Unrecognised control identifier\n";
				exit(EXIT_FAILURE);
			}
			/* remove the trailing ":" and newline */
			str_line = str_line.substr(0, str_line.length() - 2);


			if( mode == MODE_DATA )
			{
				fill_ram(elems_size, num_elems,
						 line, last_label,
						 str_line, elems);
				last_label = str_line;
				continue;
			}

			/* MODE_TEXT */
			cout << "LABEL " << line;
			if( curr_stream_length == 0 )
			{
				/* first label */
				cout << " FIRST INSTR LABEL " << endl;
				last_label = str_line;
				continue;
			}

			/* assign the instruction stream to the previous label and start
			 * anew
			 */
			streams.emplace(last_label, curr_stream);
			stream_lengths.emplace(last_label, curr_stream_length);
			uint8_t label_addr = 0;
			if( assembler.find_label(last_label, label_addr) )
			{
				/* this is a predefined label */
				if( label_addr + curr_stream_length > ROM_SIZE )
				{
					cout << "ROM Overrun" << endl;
					exit(EXIT_FAILURE);
				}
				for( uint16_t i = label_addr; i < label_addr + curr_stream_length;
					++i)
				{
					rom.used[i] = 1;
				}
			}
			else
			{
				/* unknown label - assign an address for it */
				if( curr_stream_length + rom.lfa > ROM_SIZE )
				{
					cout << "ROM Overrun: non-predef label" << endl;
					exit(EXIT_FAILURE);
				}
				assembler.add_label(last_label, rom.lfa);
				for( uint16_t i = rom.lfa; i < rom.lfa + curr_stream_length; ++i )
				{
					rom.used[i] = 1;
				}
				rom.lfa += curr_stream_length;
			}
			last_label = str_line;
			curr_stream.clear();
			curr_stream_length = 0;
// 			assembler.add_label(str_line);
		}
		/* END reading lines */
	}

	/* assign the last instruction stream to the previous label and start
	 * anew
	 */
	streams.emplace(last_label, curr_stream);
	stream_lengths.emplace(last_label, curr_stream_length);
	uint8_t label_addr = 0;
	if( assembler.find_label(last_label, label_addr) )
	{
		/* this is a predefined label */
		if( label_addr + curr_stream_length > ROM_SIZE )
		{
			cout << "ROM Overrun" << endl;
			exit(EXIT_FAILURE);
		}
		for( uint16_t i = label_addr; i < label_addr + curr_stream_length;
			++i)
			{
				rom.used[i] = 1;
			}
	}
	else
	{
		/* unknown label - assign an address for it */
		if( curr_stream_length + rom.lfa > ROM_SIZE )
		{
			cout << "ROM Overrun: non-predef label" << endl;
			exit(EXIT_FAILURE);
		}
		assembler.add_label(last_label, rom.lfa);
		for( uint16_t i = rom.lfa; i < rom.lfa + curr_stream_length; ++i )
		{
			rom.used[i] = 1;
		}
		rom.lfa += curr_stream_length;
	}
	last_label = "";
	curr_stream.clear();
	curr_stream_length = 0;




	/*--------------------------------------------------------------------------
	 * PASS1 COMPLETE
	 */
	cout << "LABELS FOLLOW------------------:" << endl;
	assembler.print_labels();



	/* all labels should be resolved now, so we can begin assembly */
	char rom_str[ROM_SIZE*3+1];
// 	char byte_str[4];
	for( int i = 0; i < ROM_SIZE*3; i += 3 )
	{
		rom_str[i] = '0';
		rom_str[i+1] = '0';
		rom_str[i+2] = '\n';
	}
	rom_str[ROM_SIZE*3] = '\0';
// 	printf( "%s", rom_str );

	map<string, vector< string > >::iterator streams_it;
	for( streams_it = streams.begin(); streams_it != streams.end(); ++streams_it)
	{
		uint16_t offset = 0;
		uint8_t start_addr = 0x00;
		cout << "Looking for label " << streams_it->first << endl;
		if( !assembler.find_label(streams_it->first, start_addr) )
		{
			cout << "Couldn't find label..." << endl;
			exit(EXIT_FAILURE);
		}
		cout << "LABEL ADDR: " << (int)start_addr << endl;
		for( uint32_t i = 0; i < streams_it->second.size(); ++i )
		{
			/* iterate over each instruction and assemble */
			cout << streams_it->second[i] << endl;
			string instr = streams_it->second[i];
			instr_t instruction = assembler.assemble(instr);
			cout << "Instruction: " << endl
				<< (int)instruction.length << " bytes long\n"
				<< (int)instruction.byte1 << endl << ""
				<< (int)instruction.byte0 << endl;
			/* TODO: sprintf */
			if( instruction.length == ONE_BYTE )
			{
				rom.val[start_addr+offset] = instruction.byte0;
				rom.used[start_addr+offset] = 1;
				++offset;
			}
			else
			{
				rom.val[start_addr+offset] = instruction.byte1;
				rom.used[start_addr+offset] = 1;
				rom.val[start_addr+offset + 1] = instruction.byte0;
				rom.used[start_addr+offset+1] = 1;
				offset += 2;
			}
		}
	}

	for( int i = 0; i < ROM_SIZE; ++i )
	{
		sprintf( rom_str + 3*i, "%02x\n", rom.val[i] );
	}
// 	printf( "%s", rom_str );
	FILE* rom_fh = fopen( "rom.hex", "w" );
	fprintf( rom_fh, "%s", rom_str );
	fclose(rom_fh);

	return 0;
}


void fill_ram(uint8_t& elems_size, uint8_t& num_elems,
			  char* line, string& last_label,
			  const string& str_line, vector< uint64_t >& elems )
{
	// write out to RAM
	uint32_t array_size = elems_size * num_elems;
	if( array_size == 0 )
	{
		cout << "LABEL " << line;
		/* this is the first label ever */
		cout << " FIRST LABEL " << endl;
		last_label = str_line;
		return;
	}
	cout << "ADDING RAM Label: " << last_label << endl;
	assembler.add_label(last_label, ram.lfa);
	cout << "ARRAY SIZE: " << array_size << endl;
	if( ram.lfa + array_size > RAM_SIZE )
	{
		cout << "Insufficient RAM, reduce memory usage" << endl;
		exit(EXIT_FAILURE);
	}

	/* write out bytes */
	cout << "NUM_ELEMS: " << (int)num_elems << endl;
	cout << "ELEMSIZE: " << (int)elems_size << endl;
	for( int i = 0; i < num_elems; ++i )
	{
		switch(elems_size)
		{
			case 1:
				ram.used[ram.lfa] = 1;
				ram.val[ram.lfa] = elems.at(i);
				++ram.lfa;
				break;
			case 2:
				ram.used[ram.lfa] = 1;
				ram.used[ram.lfa+1] = 1;
				ram.val[ram.lfa] = ( elems.at(i) & 0xff00 ) >> 8;
				ram.val[ram.lfa+1] = elems.at(i) & 0xff;
				ram.lfa += 2;
				break;
			case 4:
				ram.used[ram.lfa] = 1;
				ram.used[ram.lfa+1] = 1;
				ram.used[ram.lfa+2] = 1;
				ram.used[ram.lfa+3] = 1;
				ram.val[ram.lfa] =
				( elems.at(i) & 0xff000000 ) >> 24;
				ram.val[ram.lfa+1] =
				( elems.at(i) & 0xff0000 ) >> 16;
				ram.val[ram.lfa+2] =
				( elems.at(i) & 0xff00 ) >> 8;
				ram.val[ram.lfa+3] = elems.at(i) & 0xff;
				ram.lfa += 4;
				break;
			case 8:
				ram.used[ram.lfa] = 1;
				ram.used[ram.lfa+1] = 1;
				ram.used[ram.lfa+2] = 1;
				ram.used[ram.lfa+3] = 1;
				ram.used[ram.lfa+4] = 1;
				ram.used[ram.lfa+5] = 1;
				ram.used[ram.lfa+6] = 1;
				ram.used[ram.lfa+7] = 1;
				ram.val[ram.lfa] =
				( elems.at(i) & 0xff00000000000000 ) >> 56;
				ram.val[ram.lfa+1] =
				( elems.at(i) & 0xff000000000000 ) >> 48;
				ram.val[ram.lfa+2] =
				( elems.at(i) & 0xff0000000000 ) >> 40;
				ram.val[ram.lfa+3] =
				( elems.at(i) & 0xff00000000 ) >> 32;
				ram.val[ram.lfa+4] =
				( elems.at(i) & 0xff000000 ) >> 24;
				ram.val[ram.lfa+5] =
				( elems.at(i) & 0xff0000 ) >> 16;
				ram.val[ram.lfa+6] =
				( elems.at(i) & 0xff00 ) >> 8;
				ram.val[ram.lfa+7] = elems.at(i) & 0xff;
				ram.lfa += 8;
				break;
			default:
				cout << "ERROR, elems_size is not 1,2,4, or 8"
				<< endl;
				exit(EXIT_FAILURE);
		}
	}

	num_elems = 0;
	elems_size = 0;
	elems.clear();
	cout << "LABEL " << line;
}
