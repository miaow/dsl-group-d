/*
 * DSL Assembler
 * Copyright (C) 2014  Mihail Atanassov <mi6o.1992@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "include/asm.h"
#include<cstdio>
#include<vector>
#include <bitset>
#include<stdlib.h>
using namespace boost;


ASM::ASM()
{

}

ASM::~ASM()
{

}

bool ASM::read_instr_defs(string loc)
{
	FILE* fh = fopen(loc.c_str(), "r");
	if( fh == NULL )
	{
		return false; // file does not exist
	}

	char line[256];
	while( fgets( line, sizeof line, fh ) != NULL )
	{
		if( line[0] == '\n' || line[0] == '#' || line[0] == '\n'  )
			continue;
		/* tokenise the line. tokens are as follows:
		 * 		mnemonic
		 * 		num args
		 * 		instruction size
		 * 		[byte 1]
		 * 		byte 0
		 */
		vector<string> vec_line;
		string str_line(line);
		char_separator< char > sep(" \t"); // separate on white space
		wstokenizer_t tok(str_line, sep);
		for( wstokenizer_t::iterator it = tok.begin(); it != tok.end(); ++it)
		{
			vec_line.emplace_back(*it);
		}
// 		cout << "Reading instruction " << *tok.begin() << endl;
		/* generate an ID table entry */
		instr_dec_t ID_entry;
		ID_entry.num_args = (uint8_t)strtoul(vec_line.at(1).c_str(), NULL, 10);
// 		cout << "Num args: " << (int)ID_entry.num_args << endl;
		if(vec_line.at(2) == "1")
		{
// 			cout << "Len: 1\n";
			ID_entry.length = ONE_BYTE;
			ID_entry.byte0 = vec_line.at(3).substr(2,
												   vec_line.at(3).length() - 3);
		}
		else
		{
// 			cout << "Len: 2\n";
			ID_entry.length = TWO_BYTE;
			ID_entry.byte1 = vec_line.at(3).substr(2);
			ID_entry.byte0 = vec_line.at(4).substr(2,
												   vec_line.at(3).length() - 2);
		}
		// add it to the valid instruction pool
		ID.emplace(vec_line.at(0), ID_entry);
	}

	fclose(fh);
	return true;
}

bool ASM::read_reg_defs(string loc)
{
	FILE* fh = fopen(loc.c_str(), "r");
	if( fh == NULL )
	{
		return false; // file does not exist
	}

	char line[16];
	while( fgets( line, sizeof line, fh ) != NULL )
	{
		if( line[0] == '\0' || line[0] == '#' || line[0] == '\n'  )
			continue;

		vector<string> vec_line;
		string str_line(line);
		char_separator< char > sep(" \t"); // separate on white space
		wstokenizer_t tok(str_line, sep);
		for( wstokenizer_t::iterator it = tok.begin(); it != tok.end(); ++it)
		{
			vec_line.emplace_back(*it);
		}
		/* generate an RD table entry */
		uint8_t addr = (uint8_t)strtoul(vec_line.at(1).c_str(), NULL, 10);
		RD.emplace(vec_line.at(0), addr);
	}

	fclose(fh);
	return true;
}

bool ASM::read_label_defs(string loc)
{
	FILE* fh = fopen(loc.c_str(), "r");
	if( fh == NULL )
	{
		return false; // file does not exist
	}

	char line[256];
	while( fgets( line, sizeof line, fh ) != NULL )
	{
		if( line[0] == '\0' || line[0] == '#' || line[0] == '\n' )
			continue;

		vector<string> vec_line;
		string str_line(line);
		char_separator< char > sep(" \t"); // separate on white space
		wstokenizer_t tok(str_line, sep);
		for( wstokenizer_t::iterator it = tok.begin(); it != tok.end(); ++it)
		{
			vec_line.emplace_back(*it);
		}
		label_dec_t LD_entry;
		uint8_t addr = (uint8_t)strtoul(vec_line.at(1).substr(2).c_str(),
										NULL,
										16);
		LD_entry.addr = addr;
		LD_entry.resolved = true;
		LD.emplace(vec_line.at(0), LD_entry);
	}

	fclose(fh);
	return true;
}

instr_t ASM::assemble(string line)
{
	instr_t instruction;
	instruction.length = ONE_BYTE;
	/* check if this is only a label (for irq_0 and irq_1) */
	if( line.c_str()[1] == '@' )
	{
		cout << "@ HERE" << endl;
		cout << "looking for label " << line.substr(2, line.length()-3) << endl;
		if( !find_label(line.substr(2, line.length()-3), instruction.byte0) )
		{
			cout << "LABEL NOT FOUND DURING ASSEMBLY" << endl;
			exit(EXIT_FAILURE);
		}
		cout << "found a label: " << line.substr(1);
		cout << "addr: " << (int)instruction.byte0 << endl;
		return instruction;
	}

	/* this is a proper instruction */
	//TODO: assemble

	char_separator< char > sep(" \t"); // separate on white space
	wstokenizer_t tok(line, sep);
	wstokenizer_t::iterator it = tok.begin();
	string op = *it;
	map<string, instr_dec_t>::const_iterator ID_it;
	ID_it = ID.find(op);
	if( ID_it == ID.end() )
	{
		/* instr not found */
		cout << "OP NOT FOUND: \"" << op << "\"" << endl;
		exit(EXIT_FAILURE);
	}
	if( ID_it->second.length == ONE_BYTE )
	{
		string Rd = *(++it); // now points to first arg - target addr
		Rd = Rd.substr(0,2);
		string Rs;
		if( ID_it->second.num_args == 2 )
		{
			Rs = *(++it);
			Rs = Rs.substr(0,2);
		}
		instruction.length = ONE_BYTE;
		// TODO: one-byte instruction
		bitset<8> bits;
		cout << "BYTE0: " << ID_it->second.byte0 << endl;
		for( int i = 0; i < 8; ++i )
		{
			/* set each bit according to the instruction */
			cout << "if/else block, iteration " << i << endl;
			if( ID_it->second.byte0.substr(i,1) == "0" )
				bits[7-i] = 0;
			else if( ID_it->second.byte0.substr(i,1) == "1" )
				bits[7-i] = 1;
			else if( ID_it->second.byte0.substr(i,1) == "R" )
			{
				uint8_t reg;
				if( !find_reg(Rd, reg) )
				{
					cout << "reg not found " << *it << endl;
					exit(EXIT_FAILURE);
				}
				bits[7-i] = reg;
			}
			else if( ID_it->second.byte0.substr(i,1) == "r" )
			{
				uint8_t reg;
				if( !find_reg(Rd, reg) )
				{
					cout << "reg not found " << *it << endl;
					exit(EXIT_FAILURE);
				}
				bits[7-i] = (~reg)&1;
			}
			else if( ID_it->second.byte0.substr(i,1) == "S" )
			{
				/* TODO: source register */
				uint8_t reg;
				if( !find_reg(Rs, reg) )
				{
					cout << "reg not found " << *it << endl;
					exit(EXIT_FAILURE);
				}
				bits[7-i] = reg;
			}
			else if( ID_it->second.byte0.substr(i,1) == "s" )
			{
				/* TODO: ~source register */
				uint8_t reg;
				if( !find_reg(Rs, reg) )
				{
					cout << "reg not found " << *it << endl;
					exit(EXIT_FAILURE);
				}
				bits[7-i] = (~reg)&1;
			}
		}
		/* convert the bitset to a byte and write it out to its place */
		instruction.byte0 = (uint8_t) bits.to_ulong();
	}
	else
	{
		instruction.length = TWO_BYTE;
		cout << "BYTE1: " << ID_it->second.byte1 << endl;
		cout << "BYTE0: " << ID_it->second.byte0 << endl;
		bitset<8> bits1, bits0;
		for( int i = 0; i < 8; ++i )
		{
			/* set each bit according to the instruction */
			if( ID_it->second.byte1.substr(i,1) == "0" )
				bits1[7-i] = 0;
			else if( ID_it->second.byte1.substr(i,1) == "1" )
				bits1[7-i] = 1;
			else if( ID_it->second.byte1.substr(i,1) == "R" )
			{
				uint8_t reg;
				++it;
				if( !find_reg(*it, reg) )
					exit(EXIT_FAILURE);
				bits1[7-i] = reg;
			}
		}
		instruction.byte1 = (uint8_t) bits1.to_ulong();
		/* FIXME: byte 0 is (for now) always a memory address. don't assume this
		 * is always so.
		 */
		/* FIXME: separate data and instruction memory references */
		++it;
		if( it->substr(0,1) == "@" )
		{
			/* this is a label */
			cout << "assembling byte0 with label-deref" << endl;
			uint8_t addr;
			if( !find_label( it->substr(1, it->length()-2), addr ) )
			{
				cout << "label " << it->substr(1, it->length()-2) << " not found\n";
				exit(EXIT_FAILURE);
			}
			instruction.byte0 = addr;
		}

	}
	return instruction;
}

void ASM::print_labels()
{
	map<string, label_dec_t>::iterator it;
	for( it = LD.begin(); it != LD.end(); ++it )
	{
		cout << it->first << " " << (int) it->second.addr << endl;
	}
}

void ASM::add_label(string label, uint8_t addr)
{
	label_dec_t LD_entry;
	LD_entry.resolved = true;
	LD_entry.addr = addr;
	LD.emplace(label, LD_entry);
}

bool ASM::find_op( const string op, uint8_t& len ) const
{
	map<string, instr_dec_t>::const_iterator ID_it;
	ID_it = ID.find(op);
	if( ID_it == ID.end() )
		return false;
	len = ID_it->second.length;
	return true;
}

bool ASM::find_label( string label, uint8_t& addr ) const
{
	map<string, label_dec_t>::const_iterator LD_it;
	LD_it = LD.find(label);
	if( LD_it == LD.end() )
		return false;
	addr = LD_it->second.addr;
	return true;
}

bool ASM::find_reg( string reg, uint8_t& reg_addr ) const
{
	map<string, uint8_t>::const_iterator RD_it;
	RD_it = RD.find(reg);
	if( RD_it == RD.end() )
		return false;
	reg_addr = RD_it->second;
	return true;
}
