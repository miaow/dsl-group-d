#ifndef TYPES_H
#define TYPES_H

#include<inttypes.h>
#include<string>
#include<boost/tokenizer.hpp>

using namespace std;
using namespace boost;

#define ONE_BYTE 1
#define TWO_BYTE 2

typedef struct instr
{
	uint8_t length = 0;
	uint8_t byte1 = 0x00; /* MSB - used only if length == TWO_BYTE */
	uint8_t byte0 = 0x00; /* LSB */
} instr_t;

typedef struct instr_dec
{
	uint8_t length = 0;
	uint8_t num_args = 0;
	string byte1;
	string byte0;
} instr_dec_t;

typedef struct label_dec
{
	bool resolved = false;
	uint8_t addr = 0x00;
	uint8_t offset_mul = 0x00;
} label_dec_t;

typedef tokenizer< char_separator<char> > wstokenizer_t;

#endif /* TYPES.H */
