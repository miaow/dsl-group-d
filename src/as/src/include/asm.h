/*
 * DSL Assembler
 * Copyright (C) 2014  Mihail Atanassov <mi6o.1992@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ASM_H
#define ASM_H

#include<string>
#include<map>
#include<inttypes.h>
#include "types.h"

#define RAM_SIZE 128
#define ROM_SIZE 256

using namespace std;

class ASM
{

private:
	map<string, instr_dec_t> ID; /* instruction decoding map */
	map<string, uint8_t> RD; /* register decoding map */
	map<string, label_dec_t> LD; /* label decoding map */

public:
	ASM();
	~ASM();

	bool find_op( const string, uint8_t& ) const;
	bool find_label( const string, uint8_t& ) const;
	bool find_reg( const string, uint8_t& ) const;

	bool read_instr_defs(string loc);
	bool read_reg_defs(string loc);
	bool read_label_defs(string loc);
	instr_t assemble(string line);
	void add_label(string label, uint8_t addr);

	void print_labels();
};

#endif // ASM_H
