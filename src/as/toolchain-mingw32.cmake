# the name of the target operating system
SET(CMAKE_SYSTEM_NAME Windows)

# which compilers to use for C and C++
SET(CMAKE_C_COMPILER /usr/bin/i686-w64-mingw32-gcc)
SET(CMAKE_CXX_COMPILER /usr/bin/i686-w64-mingw32-g++)
SET(CMAKE_RC_COMPILER /usr/bin/i686-w64-mingw32-windres)

SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -static-libgcc")
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -static-libgcc -static-libstdc++")
SET(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS 
	"${CMAKE_SHARED_LIBRARY_LINK_C_FLAGS} -static-libgcc -s")
SET(CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS 
	"${CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS} -static-libgcc -static-libstdc++ -s")
	
SET(CMAKE_EXE_LINKER_FLAGS "-static")

# here is the target environment located
SET(CMAKE_FIND_ROOT_PATH  /usr/i686-mingw32 /home/mihail/mingw-install )

# adjust the default behaviour of the FIND_XXX() commands:
# search headers and libraries in the target environment, search 
# programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)