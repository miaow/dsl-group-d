`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
//
// Create Date:		10:08:45 01/30/2014
// Design Name:		PS/2 Demo
// Module Name:		bus_buttons
// Project Name:	PS/2 Demo
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		The PS/2 module - complete, bus-wrapped.
//
// Dependencies:	globals.v
//
// Revision:
// v1.00 - 20140327xxxxZ - Verified
// v0.02 - 20140327xxxxZ - Implementation done
// v0.01 - 201403270936Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////
`include "./globals.v"
`define BUTTONS_ADDR 8'h80

module bus_buttons
(
	/* standard inputs */
	input			CLK,
	input			RESET,
	/* bus I/O */
	output	[7:0]	BUS_DATA,
	input	[7:0]	BUS_ADDR,
	input			BUS_DATA_OE,
	/* global inputs */
	input	[7:0]	BUTTONS
);

	reg		[7:0]	BUS_DATA_OUT;
	reg				BUS_BUTTONS_OE;
	assign BUS_DATA = BUS_BUTTONS_OE ? BUS_DATA_OUT : 8'hzz;

	always@( posedge CLK or posedge RESET ) begin
		if( RESET ) begin
			BUS_DATA_OUT	<= 8'h00;
			BUS_BUTTONS_OE	<= `FALSE;
		end
		else begin
			if( BUS_ADDR == `BUTTONS_ADDR && ~BUS_DATA_OE ) begin
				BUS_DATA_OUT	<= BUTTONS;
				BUS_BUTTONS_OE	<= `TRUE;
			end
			else begin
				BUS_BUTTONS_OE	<= `FALSE;
			end
		end
	end

endmodule
