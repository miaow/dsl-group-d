`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company: 		UoE
// Engineers:		Mihail Atanassov
//					Huiying Liu
//					Roy Kong
//
// Create Date:		09:32:02 10/17/2012
// Design Name: 	DSL
// Module Name:		dsl_grp_d
// Project Name:	DSL Group D
// Target Devices:	Digilent Basys2 100K
// Tool versions:	Xilinx ISE WebPack 14.7 64-bit
//					Digilent JTAG Config Utility v2.1.1 Linux
// Description:		The complete DSL project.
// Dependencies:	50MHz clock
//			bus_s7dec.v -- 7-segment display decoder module
//			bus_leds.v -- LED driver
//			mcu.v -- 8-bit MCU
//			ram.v -- single-port RAM module
//			rom.v -- single-port ROM
//			timer.v -- reconfigurable timer
//			bus_ps2.v -- PS/2 driver
//			IRTransmitterSM.v -- IR diode driver
//			VGA_top.v -- VGA driver
//			if PS2_DEBUG is active - ICON and ILA IP cores from Xilinx
//
//	Interface:
//		CLK:		Clock input. 50MHz oscillator at M6 (ext) or B8 (int).
//		RESET:		Button input that resets the counters to 0.
//		IO_CLK:		Clock line for the PS/2 port.
//		IO_DAT:		Data line for the PS/2 port.
//		SEG_SELECT:	Select line for the 7-segment display. Connected
//					to AN1 through to AN4.
//		DEC_OUT:	Data line for the 7-segment display. Connected
//					to CA through to CG and DP.
//		LED_OUT:	Output bus for the on-board LEDs.
//		BUTTON:		Input to control whether to activate scrollwheel support in
//					the PS/2 mouse.
//		IR_SELECT:	4-bit wide bus (single-hot encoded) that selects which car
//					colour to control.
//		IR_LED:		Infrared LED output line.
//		VGA_COLOUR:	8-bit bus outputting the RGB color value over the DB-15
//					connector
//		VGA_HS:		Horizontal sync line for the VGA output.
//		VGA_VS:		Vertical sync line for the VGA output.
////////////////////////////////////////////////////////////////////////////////
`include "globals.v"

module dsl_grp_d
(
	input			CLK,
	input			RESET,
	/* PS/2 I/O */
	inout			IO_CLK,
	inout			IO_DAT,
	/* 7-seg o/p */
	output	[3:0]	SEG_SELECT,
	output	[7:0]	DEC_OUT,
	/* LEDs o/p */
	output	[7:0]	LED_OUT,
	/* button i/ps to ctrl LED o/p & mouse scrollwheel support */
	input	[7:0]	BUTTONS,
	/* IR */
	output			IR_LED,
	/* VGA */
	output	[7:0]	VGA_COLOUR,
	output			VGA_HS,
	output			VGA_VS
);

	/*--------------------------------------------------------------------------
	 * system interconnect
	 */
	wire	[7:0]	bus_data;
	wire	[7:0]	bus_addr;
	wire			bus_data_oe;
	wire	[1:0]	irq, ack;

	/*--------------------------------------------------------------------------
	 * human interface
	 */
	/* the bus-based 7-segment decoder module
	 * input-only bus_data, so no need for bus_data_oe line
	 */
	bus_s7dec s7d
	(
		.CLK(CLK),
		.RESET(RESET),
		.BUS_DATA(bus_data),
		.BUS_ADDR(bus_addr),
		.SEG_SELECT(SEG_SELECT),
		.HEX_OUT(DEC_OUT)
	);


	/* bus-based LED driver
	 * same as with the 7-seg module, no need for a write-enable line
	 */
	bus_leds led
	(
		.CLK(CLK),
		.RESET(RESET),
		.BUS_DATA(bus_data),
		.BUS_ADDR(bus_addr),
		.LED_OUT(LED_OUT)
	);


	/* bus-based buttons driver */
	bus_buttons btns
	(
		.CLK(CLK),
		.RESET(RESET),
		.BUS_DATA(bus_data),
		.BUS_ADDR(bus_addr),
		.BUS_DATA_OE(bus_data_oe),
		.BUTTONS(BUTTONS)
	);


	/*--------------------------------------------------------------------------
	 * bus-based PS/2 driver
	 */
`ifdef PS2_DEBUG
	wire	[7:0]	dbg_tx_byte;
	wire	[7:0]	dbg_rx_byte;
	wire	[5:0]	dbg_state;
	wire	[1:0]	dbg_rx_err;
`endif

	bus_ps2 ps2drv
	(
`ifdef PS2_DEBUG
		/* debug outputs */
		.DBG_TX_BYTE(dbg_tx_byte),
		.DBG_RX_BYTE(dbg_rx_byte),
		.DBG_STATE(dbg_state),
		.DBG_RX_ERR(dbg_rx_err),
`endif
		.CLK(CLK),
		.RESET(RESET),

		.BUS_DATA(bus_data),
		.BUS_ADDR(bus_addr),
		.BUS_DATA_OE(bus_data_oe),
		.BUS_IRQ(irq[0]),
		.BUS_ACK(ack[0]),

		.IO_CLK(IO_CLK),
		.IO_DAT(IO_DAT)
	);


	/*--------------------------------------------------------------------------
	 * IR transmitter driver
	 */
	IRTransmitterSM ir_drv
	(
		.CLK(CLK),					/* CLK signal */
		.RESET(RESET),				/* RESET signal */
		.BUS_DATA(bus_data),		/* predefined data from RAM to IR */
		.BUS_ADDR(bus_addr),		/* address bus for IR (8'h90) */
		.BUS_DATA_OE(bus_data_oe),	/* write enable signal from mcu */
		.IR_LED(IR_LED)				/* output to IR_LED */
	);


	/*--------------------------------------------------------------------------
	 * VGA driver
	 */
	VGA_top vga_drv
	(
		.CLK(CLK),
		.RESET(RESET),
		.BUS_ADDR(bus_addr),
		.BUS_DATA(bus_data),
		.BUS_DATA_OE(bus_data_oe),
		.VGA_COLOUR(VGA_COLOUR),
		.VGA_VS(VGA_VS),
		.VGA_HS(VGA_HS)
	);


	/*--------------------------------------------------------------------------
	 * MCU + RAM & ROM + timer instances
	 */
	wire	[7:0]	rom_addr, rom_data;

	mcu mcu
	(
		.CLK(CLK),
		.RESET(RESET),
		/* bus signals */
		.BUS_DATA(bus_data),
		.BUS_ADDR(bus_addr),
		.BUS_DATA_OE(bus_data_oe),
		.BUS_IRQ(irq),
		.BUS_ACK(ack),
		/* rom interconnect */
		.ROM_ADDR(rom_addr),
		.ROM_DATA(rom_data)
	);

	rom rom
	(
		.CLK(CLK),
		/* bus signals */
		.ROM_DATA(rom_data),
		.ROM_ADDR(rom_addr)
	);

	ram ram
	(
		.CLK(CLK),
		/* bus signals */
		.BUS_DATA(bus_data),
		.BUS_ADDR(bus_addr),
		.BUS_DATA_OE(bus_data_oe)
	);

	timer tim
	(
		.CLK(CLK),
		.RESET(RESET),
		/* bus signals */
		.BUS_DATA(bus_data),
		.BUS_ADDR(bus_addr),
		.BUS_DATA_OE(bus_data_oe),
		.BUS_IRQ(irq[1]),
		.BUS_ACK(ack[1])
	);

`ifdef PS2_DEBUG
	/* the chipscope instance */
	/* ICON instance. Use default settings for core generation */
	wire	[35:0]	ctrl_bus;
	icon	i_ctrl
	(
		.CONTROL0(ctrl_bus)
	);
	/* ILA instance. Requirements for core generation:
	 * Number of triggers: 6
	 * Trigger 0 width: 1
	 * Trigger 1 width: 1
	 * Trigger 2 width: 6
	 * Trigger 3 width: 8
	 * Trigger 4 width: 8
	 * Trigger 5 width: 2
	 *
	 * Amount of samples: for the Basys2, the maximum that can fit is 2048
	 */
	ila		i_la
	(
		.CONTROL(ctrl_bus),
		.CLK(IO_CLK),
		.TRIG0(IO_DAT),
		.TRIG1(RESET),
		.TRIG2(dbg_state),
		.TRIG3(dbg_tx_byte),
		.TRIG4(dbg_rx_byte),
		.TRIG5(dbg_rx_err)
	);
`endif

endmodule
