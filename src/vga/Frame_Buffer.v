`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Huiying Liu
//
// Create Date:    18:46:23 03/02/2014
// Design Name:    Microprocess control VGA
// Module Name:    mcu_top
// Project Name:   dsl_grp_d
// Target Devices: xc3s100e-4cp132
// Tool versions:  xilinx ISE design tool 14.7
// Description:    Display chequer color on the VGA screen controlled by microprocess
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module Frame_Buffer
(
	A_CLK,
	A_ADDR_X,
	A_ADDR_Y,
	A_DATA_IN,
	A_DATA_OUT,
	A_WE,
	B_CLK,
	B_ADDR_X,
	B_ADDR_Y,
	B_DATA_OUT
);
/// Port A - Read/Write
input             A_CLK;
input      [7:0] A_ADDR_X;  // 8 + 7 bits = 15 bits hence [14:0]
input      [6:0] A_ADDR_Y;  // 8 + 7 bits = 15 bits hence [14:0]
input             A_DATA_IN; // Pixel Data In
output reg       A_DATA_OUT;
input             A_WE;

//Port B - Read Only  // Write Enable
input             B_CLK;
input      [7:0] B_ADDR_X; // Pixel Data Out
input      [6:0] B_ADDR_Y; // Pixel Data Out
output reg       B_DATA_OUT;
wire	[14:0]	a_mem_index;
assign a_mem_index = {A_ADDR_Y, A_ADDR_X};
wire	[14:0]	b_mem_index;
assign b_mem_index = {B_ADDR_Y, B_ADDR_X};

// A 160 x 12 1-bit memory to hold frame data
//The LSBs of the address correspond to the X axis, and the MSBs to the Y axis
reg [0:0] mem [0:2**15-1];
initial $readmemb("fb.bin", mem);

// Port A - Read/Write e.g. to be used by microprocessor
	always@(posedge A_CLK) begin
		if(A_WE) begin
			mem[a_mem_index] <=  A_DATA_IN;
		end
		A_DATA_OUT  <=  mem[a_mem_index];
	end

// Port B - Read Only e.g. to be read from the VGA module for displaying
	always@(posedge B_CLK) begin
		B_DATA_OUT <= mem[b_mem_index];
	end

endmodule
