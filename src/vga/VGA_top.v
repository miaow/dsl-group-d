`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Huiying Liu
//
// Create Date:    18:46:23 03/02/2014
// Design Name:    Microprocess control VGA
// Module Name:    mcu_top
// Project Name:   dsl_grp_d
// Target Devices: xc3s100e-4cp132
// Tool versions:  xilinx ISE design tool 14.7
// Description:    Display checkered color on the VGA screen controlled by
//					microprocess
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
////////////////////////////////////////////////////////////////////////////////
`include "../globals.v"

module VGA_top
(
	/* standard inputs */
	CLK,
	RESET,
	/* bus I/O */
	BUS_DATA,
	BUS_ADDR,
	BUS_DATA_OE,
	/* global outputs */
	VGA_COLOUR,
	VGA_VS,
	VGA_HS
);

	parameter write_max = 2;

	input			CLK;
	input			RESET;
	inout	[7:0]	BUS_DATA;
	input	[7:0]	BUS_ADDR;
	input			BUS_DATA_OE;
	// pins for VGA interface
	output	[7:0]	VGA_COLOUR;
	output			VGA_HS;
	output			VGA_VS;

	/* Framebuffer control registers - MCU-side */
	reg		[7:0]	pc_addr_x;
	reg		[6:0]	pc_addr_y;
	reg				pc_pixel_in;
	wire			framebuf_dat_out;

	// microprocessor interface
	reg				BUS_VGA_OE;
	reg		[7:0]	BUS_DATA_OUT;
	wire	[7:0]	BUS_DATA_IN;
	assign BUS_DATA_IN = BUS_DATA;
	assign BUS_DATA = BUS_VGA_OE ? BUS_DATA_OUT : 8'hzz;

	/* state register */
	reg		[1:0]	state_reg;
	// background and foreground colour define
	reg		[15:0]	config_colour;

	/* write enable signal for the frame buffer */
	reg				framebuf_we;


	always @( posedge CLK or posedge RESET )begin
		if( RESET)begin
			state_reg <= 'd0;
		end
		else if( BUS_ADDR == 8'hB0 & BUS_DATA_OE ) begin
			state_reg <= BUS_DATA_IN[1:0];
		end
		else begin
			state_reg <= state_reg;
		end
	end

	/*
	 * BUS_ADDR:
	 * 	B0:	Y address;
	 * 	B1:	X address;
	 * 	B2:	read: return mem[X][Y];
	 * 		write: store ~BUS_DATA_IN[0] into mem[X][Y].
	 */

	/* small state machine for the BUS_ADDR control */
	always@(posedge CLK,posedge RESET) begin
		if(RESET)begin
			pc_addr_x <= 8'h50;
			pc_addr_y <= 7'h3C;
			pc_pixel_in <= 1'b0;
			config_colour <= 16'ha800;
		end
		else begin
			if(BUS_DATA_OE) begin
				framebuf_we			<= `FALSE;
				BUS_VGA_OE			<= `FALSE;
				case(state_reg)
					2'b00: begin
						case(BUS_ADDR)
							8'hB1:begin // y address
								pc_addr_y <= BUS_DATA_IN[6:0];
							end
							8'hB2:begin // x address
								pc_addr_x <= BUS_DATA_IN[7:0];
							end
							default:;
						endcase
					end
					2'b01: begin
						if(BUS_ADDR == 8'hB1)begin
							pc_pixel_in <= ~BUS_DATA_IN[0];
							framebuf_we	<= `TRUE;
						end
					end
					2'b10: begin
						case(BUS_ADDR)
							8'hB1:begin // foreground colour
								config_colour[15:8] <= BUS_DATA_IN;
							end
							8'hB2:begin // background colour
								config_colour[7:0] <= BUS_DATA_IN;
							end
							default:;
						endcase
					end
				endcase
// 				case(BUS_ADDR)
// 					8'hB0: begin //y_address
// 						pc_addr_y <= BUS_DATA_IN[6:0];
// 					end
// 					8'hB1: begin
// 						pc_addr_x <= BUS_DATA_IN[7:0];
// 					end
// 					8'hB2: begin
// 						pc_pixel_in <= ~BUS_DATA_IN[0];
// 						framebuf_we	<= `TRUE;
// 					end
// 				endcase
			end
			else begin /* ~BUS_DATA_OE */
				framebuf_we			<= `FALSE;
				if( BUS_ADDR == 8'hB1 && state_reg == 2'b01 ) begin
					BUS_DATA_OUT	<= framebuf_dat_out;
					BUS_VGA_OE		<= `TRUE;
				end
				else begin
					BUS_VGA_OE		<= `FALSE;
				end
				pc_addr_x <= pc_addr_x;
				pc_addr_y <= pc_addr_y;
				pc_pixel_in <= pc_pixel_in;
				config_colour	<= config_colour;
			end
		end
	end

	/* framebuffer interconnect */
	wire	[7:0]	vga_address_x;
	wire	[6:0]	vga_address_y;
	wire			vga_colour_ctrl;
	wire			div_clk;

	// VGA control module
	VGA_SIG_GEN VGA_ctrl
	(
		.CLK(CLK),
		.RESET(RESET),
		.CONFIG_COLOURS(config_colour),
		.DPR_CLK(div_clk),
		.VGA_ADDR_X(vga_address_x),
		.VGA_ADDR_Y(vga_address_y),
		.VGA_DATA(vga_colour_ctrl),  //PIXEL control input
		.VGA_HS(VGA_HS),
		.VGA_VS(VGA_VS),
		.VGA_COLOUR(VGA_COLOUR)
	);

	// Frame buffer module
	Frame_Buffer Frame_buf
	(
		.A_CLK(CLK),
		.A_ADDR_X(pc_addr_x),
		.A_ADDR_Y(pc_addr_y),
		.A_DATA_IN(pc_pixel_in),
		.A_DATA_OUT(framebuf_dat_out),
		.A_WE(framebuf_we),
		.B_CLK(div_clk),
		.B_ADDR_X(vga_address_x),
		.B_ADDR_Y(vga_address_y),
		.B_DATA_OUT(vga_colour_ctrl)
	);

endmodule
