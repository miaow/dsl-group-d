`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Huiying Liu
//
// Create Date:    18:46:23 03/02/2014
// Design Name:    Microprocess control VGA
// Module Name:    mcu_top
// Project Name:   dsl_grp_d
// Target Devices: xc3s100e-4cp132
// Tool versions:  xilinx ISE design tool 14.7
// Description:    Display chequered color on the VGA screen controlled by
//					microprocessor
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
////////////////////////////////////////////////////////////////////////////////
module VGA_SIG_GEN
(
	CLK,
	RESET,
	CONFIG_COLOURS,
	DPR_CLK,
	VGA_ADDR_X,
	VGA_ADDR_Y,
	VGA_DATA,
	VGA_HS,
	VGA_VS,
	VGA_COLOUR
);

	input				CLK;
	input				RESET;
	//Colour Configuration Interface
	input		[15:0]	CONFIG_COLOURS;
	// Frame Buffer (Dual Port memory) Interface
	output				DPR_CLK;
	output		[7:0]	VGA_ADDR_X;
	output		[6:0]	VGA_ADDR_Y;
	input				VGA_DATA;
	//VGA Port Interface
	output reg			VGA_HS;
	output reg			VGA_VS;
	output reg	[7:0]	VGA_COLOUR;



	reg			[9:0]	Addr_x;
	reg			[8:0]	Addr_y;
	reg			[15:0]	buf__config_colours;

	always@( negedge VGA_VS or posedge RESET ) begin
	if( RESET ) begin
		buf__config_colours	<= 16'ha800;
	end
	else begin
		buf__config_colours	<= CONFIG_COLOURS;
	end
end

	// Define VGA signal parameters e.g. Horizontal and Vertical display time,
	// pulse widths, front and back porch widths etc.

	// Use the following signal parameters
	parameter HTs		= 800; // Total Horizontal Sync Pulse Time
	parameter HTpw		= 96; // Horizontal Pulse Width Time
	parameter HTDisp	= 649; // Horizontal Display Time
	parameter Hbp		= 46; // Horizontal Back Porch Time
	parameter Hfp		= 9; // Horizontal Front Porch Time
	parameter VTs		= 521; // Total Vertical Sync Pulse Time
	parameter VTpw		= 2; // Vertical Pulse Width Time
	parameter VTDisp	= 480; // Vertical Display Time
	parameter Vbp		= 29; // Vertical Back Porch Time
	parameter Vfp		= 10; // Vertical Front Porch Time

	//Halve the clock to 25MHz to drive the VGA display
	reg VGA_CLK;
	always@(posedge CLK) begin
		if(RESET)
			VGA_CLK <= 0;
		else
			VGA_CLK <= ~VGA_CLK;
	end

	//Horizontal Counter
	reg		[9:0]	HCounter;
	always@(posedge VGA_CLK)begin : H_CNT
		if(RESET)
			HCounter <= 'd0;
		else if(HCounter < HTs - 1)
			HCounter <= HCounter + 1'b1;
		else
			HCounter <= 'd0;
	end

	//Vertical Counter
	reg		[9:0]	VCounter;
	always@(posedge VGA_CLK)begin : V_CNT
		if(RESET)begin
			VCounter <= 'd0;
		end
		else if(HCounter == HTs - 1) begin
			if(VCounter < VTs - 1)
				VCounter <= VCounter + 1'b1;
			else
				VCounter <= 'd0;
		end
		else
			VCounter <= VCounter;
	end

	/* Create a process that generates the horizontal and vertical sync
	 * signals, as well as the pixel colour information, using HCounter and
	 * VCounter. Do not forget to use CONFIG_COLOURS input to display the right
	 * foreground and background colours.
	 */

	//Horizontal synchronous pulse
	always@(posedge VGA_CLK) begin
		if(RESET)
			VGA_HS <= 1;
		else begin
			if(HCounter < HTpw)
				VGA_HS <= 0;
			else
				VGA_HS <= 1;
		end
	end

	//Vertical synchronouse pulse
	always@(posedge VGA_CLK) begin
		if(RESET)
			VGA_VS <= 1;
		else begin
			if(VCounter < VTpw)
				VGA_VS <= 0;
			else
				VGA_VS <= 1;
		end
	end


	/* Finally, tie the output of the frame buffer to the colour output. */
	//Define the colour signal
	always@(posedge VGA_CLK) begin
		if(RESET)
			VGA_COLOUR <= 8'b00000000;
		else begin
			if( ( HCounter >= ( HTs - HTDisp - Hfp ) ) &&
				( HCounter <= ( HTs - Hfp ) ) &&
				( VCounter >= ( VTs - VTDisp - Vfp ) ) &&
				( VCounter <= ( VTs - Vfp ) ) ) begin
				if(VGA_DATA)
					VGA_COLOUR <= buf__config_colours[15:8];
				else
					VGA_COLOUR <= buf__config_colours[7:0];
			end
			else
				VGA_COLOUR <= 8'b00000000;
		end
	end


	/* Need to create the address of the next pixel. Concatenate and tie the
	 * look-ahead address to the frame buffer address.
	 */

	always@(posedge VGA_CLK) begin
		if(RESET) begin
			Addr_x <= 0;
			Addr_y <= 0;
		end
		else begin
			if( ( HCounter >= ( HTs - HTDisp - Hfp ) ) &&
				( HCounter <= ( HTs - Hfp ) ) &&
				( VCounter >= ( VTs - VTDisp - Vfp ) ) &&
				( VCounter <= ( VTs - Vfp ) ) ) begin
				Addr_x <= HCounter - (HTs - HTDisp - Hfp);
				Addr_y <= VCounter - (VTs - VTDisp - Vfp);
			end
			else begin
				Addr_x <= 0;
				Addr_y <= 0;
			end
		end
	end


   assign VGA_ADDR_X = Addr_x[9:2];
   assign VGA_ADDR_Y = Addr_y[8:2];
   assign DPR_CLK = VGA_CLK;



endmodule
