`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   09:39:34 03/04/2014
// Design Name:   mcu_top
// Module Name:   /media/My Passport/Git/DSL4_groupD/dsl-group-d/mcu_top_tb.v
// Project Name:  dsl_grp_d
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: mcu_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module mcu_top_tb;

	// Inputs
	reg CLK;
	reg RESET;


	// Bidirs
	wire [7:0] BUS_DATA;

	// Instantiate the Unit Under Test (UUT)
	mcu_top uut (
		.CLK(CLK), 
		.RESET(RESET)
	);
	
	
   //define the clock here
	always
	  #10  CLK = !CLK	;
	
	

	initial begin
//	   $readmemh( "rom.hex", rom_data );
		// Initialize Inputs
		CLK = 0;
		RESET = 0;


		// Wait 100 ns for global reset to finish
		#100;
      #150 RESET = 1'b1;
      #150 RESET = 1'b0;		
		
        
		  
		
		// Add stimulus here

	end
      
endmodule

