`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Huiying Liu
// 
// Create Date:    18:46:23 03/02/2014 
// Design Name:    Microprocess control VGA
// Module Name:    mcu_top 
// Project Name:   dsl_grp_d 
// Target Devices: xc3s100e-4cp132
// Tool versions:  xilinx ISE design tool 14.7
// Description:    Display chequer color on the VGA screen controlled by microprocess
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mcu_top(CLK,
               RESET,
										
					VGA_COLOUR,
					VGA_VS,
					VGA_HS
									
    );
	 
	
 input CLK;
 input RESET;
 
 output [7:0] VGA_COLOUR;
 output VGA_VS;
 output VGA_HS;

//wire connection	 
 wire [7:0] bus_addr_transfer;
 wire [7:0] addr_transfer;
 wire [7:0] bus_data_transfer;
 wire       bus_data_w_en;
 
 wire [7:0] rom_data_transfer;
 
 wire [1:0] bus_irq_sig;
 wire [1:0] bus_ack_sig;
 
 //assign bus_addr_transfer =  rom_data_transfer;
   
// microprocess top module
mcu mcu_ctrl(
    .CLK(CLK), 
    .RESET(RESET), 
    .BUS_DATA(bus_data_transfer), 
    .BUS_ADDR(bus_addr_transfer), 
    .BUS_DATA_OE(bus_data_w_en), 
    .BUS_IRQ(bus_irq_sig), 
    .BUS_ACK(bus_ack_sig), 
    .ROM_ADDR(addr_transfer), 
    .ROM_DATA(rom_data_transfer)
    );
	 
	 
	 
// Rom memory module
rom mem_rom(
    .CLK(CLK), 
    .BUS_DATA(rom_data_transfer), 
    .BUS_ADDR(addr_transfer)
    );

// Ram memory module
ram mem_ram(
    .CLK(CLK), 
    .BUS_DATA(bus_data_transfer), 
    .BUS_ADDR(bus_addr_transfer), 
    .BUS_WE(bus_data_w_en)
    );


// VGA_top module
VGA_top vga_top(
    .CLK(CLK), 
    .RESET(RESET), 
    .PC_W_EN(bus_data_w_en), 
    .PC_DATA_OUT(PC_DATA_OUT), 
    .BUS_ADDR(bus_addr_transfer), 
    .BUS_DATA(bus_data_transfer), 
    .VGA_COLOUR(VGA_COLOUR), 
    .VGA_VS(VGA_VS), 
    .VGA_HS(VGA_HS)
    );

// Timer module
timer interrupter (
    .CLK(CLK), 
    .RESET(RESET), 
    .BUS_DATA(bus_data_transfer), 
    .BUS_ADDR(bus_addr_transfer), 
    .BUS_WE(bus_data_w_en), 
    .BUS_IRQ(bus_irq_sig[0]), 
    .BUS_ACK(bus_ack_sig[0])
    );


endmodule
