`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// Company:			UoE
// Engineer:		Mihail Atanassov
//
// Create Date:		15:10:33 01/30/2014
// Design Name:		MCU
// Module Name:		rom
// Project Name:	dsl_group_d
// Target Devices:	Digilent Basys 2
// Tool versions:	ISE WebPack 14.7
// Description:		The ROM for the MCU.
//
// Dependencies:	none
//
// Revision:
// v0.02 - 201401312031Z - Implementation complete
// v0.01 - 201401312027Z - File Created
// Additional Comments: ---
//
////////////////////////////////////////////////////////////////////////////////
`include "globals.v"

module rom
(
	input				CLK,
	/* bus signals */
	output reg	[7:0]	ROM_DATA,
	input		[7:0]	ROM_ADDR
);

	parameter rom_addr_width	= 8;

	/* memory */
	reg 		[7:0] 	rom		[2**rom_addr_width-1:0];

	/* load program */
	initial $readmemh( "rom.hex", rom );

	/* single read port */
	always @( posedge CLK ) begin
		ROM_DATA				<= rom[ROM_ADDR];
	end

endmodule